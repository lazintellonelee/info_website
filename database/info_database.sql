-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2015 at 06:11 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `info_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE IF NOT EXISTS `tbl_admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_prefix` enum('subsidiary','primary') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'subsidiary',
  `admin_role_id` int(1) NOT NULL,
  `admin_active` int(1) NOT NULL DEFAULT '1',
  `admin_last_activity` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `admin_username`, `admin_password`, `admin_salt`, `admin_prefix`, `admin_role_id`, `admin_active`, `admin_last_activity`, `updated_at`, `created_at`) VALUES
(1, 'admin', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'primary', 1, 1, '2015-09-26 10:34:27', '2015-09-26 10:34:27', '2015-09-11 06:51:45'),
(2, 'lehuyvu', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'subsidiary', 2, 1, '2015-09-24 01:31:42', '2015-09-24 01:31:42', '2015-09-11 06:51:45'),
(3, 'subadmin', 'c75bb00cf95669b32656b9c816ebbbb0', '820a20eb63d2774dd4aee24c74f661e3', 'subsidiary', 3, 1, '2015-09-24 01:31:27', '2015-09-24 01:31:27', '2015-09-23 15:52:10'),
(4, 'tucuibep', 'b7bba3fae2ba97c69cd9c5e8061fd4ad', '8076715e6a8a94c81881aafb6ec8ac19', 'subsidiary', 3, 1, NULL, '2015-09-24 11:37:10', '2015-09-24 04:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_roles`
--

CREATE TABLE IF NOT EXISTS `tbl_admin_roles` (
  `admin_role_id` int(11) NOT NULL,
  `admin_role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_role_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin_roles`
--

INSERT INTO `tbl_admin_roles` (`admin_role_id`, `admin_role_name`, `admin_role_title`, `updated_at`, `created_at`) VALUES
(1, 'all', 'Administrator', NULL, '2015-09-11 07:54:02'),
(2, 'product_manager', 'Product Manager', NULL, '2015-09-22 18:56:10'),
(3, 'news_manager', 'News Manager', NULL, '2015-09-23 15:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_configuration`
--

CREATE TABLE IF NOT EXISTS `tbl_home_configuration` (
  `home_configuration_id` int(10) unsigned NOT NULL,
  `home_configuration_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_shortcut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_meta_author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_configuration_refresh_state` int(1) NOT NULL DEFAULT '0',
  `home_configuration_refresh_second` int(10) unsigned NOT NULL DEFAULT '0',
  `home_facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_home_configuration`
--

INSERT INTO `tbl_home_configuration` (`home_configuration_id`, `home_configuration_title`, `home_configuration_shortcut`, `home_configuration_logo`, `home_configuration_meta_description`, `home_configuration_meta_keywords`, `home_configuration_meta_author`, `home_configuration_refresh_state`, `home_configuration_refresh_second`, `home_facebook`, `home_google`, `home_twitter`, `updated_at`, `created_at`) VALUES
(1, 'Info Website', 'public/upload/images/configuration/shortcut-icon.png', 'public/upload/images/configuration/logo.png', 'Info Website', 'Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website, Info Website', 'Vu Lee', 1, 60, 'http://facebook.com', 'http://google.com', 'http://twitter.com', '2015-09-22 01:54:29', '2015-09-21 08:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE IF NOT EXISTS `tbl_products` (
  `product_id` int(10) unsigned NOT NULL,
  `product_type_id` int(10) unsigned NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `product_large_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_small_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` double unsigned NOT NULL,
  `product_old_price` double unsigned DEFAULT NULL,
  `product_quantity` int(10) unsigned NOT NULL,
  `product_label` enum('new','hot','promotion') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
  `product_promotion` int(10) unsigned DEFAULT NULL,
  `product_view` int(10) unsigned NOT NULL DEFAULT '0',
  `product_active` int(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_type_id`, `product_name`, `product_detail`, `product_large_image`, `product_small_image`, `product_price`, `product_old_price`, `product_quantity`, `product_label`, `product_promotion`, `product_view`, `product_active`, `created_by`, `updated_at`, `created_at`) VALUES
(9, 38, 'Ốc sên', 'Ba con&nbsp;ốc sên', 'public/upload/images/product/56017e0aa3151_Oc_sen.jpg', 'public/upload/images/product/thumb_56017e0aa3151_Oc_sen.jpg', 100000, 100000, 3, 'hot', NULL, 0, 1, 1, '2015-09-23 01:32:25', '2015-09-22 16:12:58'),
(10, 39, 'Đỗ Nguyễn Minh Trang ', 'Đỗ Nguyễn Minh Trang&nbsp;', 'public/upload/images/product/560189323f94e_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_560189323f94e_Do_Nguyen_Minh_Trang_.jpg', 123, 0, 121, 'hot', NULL, 0, 1, 1, '2015-09-26 08:48:17', '2015-09-22 17:00:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_histories`
--

CREATE TABLE IF NOT EXISTS `tbl_product_histories` (
  `product_history_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  `product_history_action` enum('active','unactive','edit','delete','restore') COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_images`
--

CREATE TABLE IF NOT EXISTS `tbl_product_images` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_image_large` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_image_small` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product_images`
--

INSERT INTO `tbl_product_images` (`product_image_id`, `product_id`, `product_image_large`, `product_image_small`, `updated_at`, `created_at`) VALUES
(30, 9, 'public/upload/images/product/56017e0ac400d_image_Oc_sen.jpg', 'public/upload/images/product/thumb_56017e0ac400d_image_Oc_sen.jpg', '2015-09-22 23:12:58', '2015-09-22 16:12:58'),
(31, 10, 'public/upload/images/product/56018932de2f9_image_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_56018932de2f9_image_Do_Nguyen_Minh_Trang_.jpg', '2015-09-23 00:00:35', '2015-09-22 17:00:35'),
(32, 10, 'public/upload/images/product/560189337bb94_image_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_560189337bb94_image_Do_Nguyen_Minh_Trang_.jpg', '2015-09-23 00:00:36', '2015-09-22 17:00:36'),
(33, 10, 'public/upload/images/product/560189341c0ef_image_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_560189341c0ef_image_Do_Nguyen_Minh_Trang_.jpg', '2015-09-23 00:00:36', '2015-09-22 17:00:36'),
(34, 10, 'public/upload/images/product/56018934baf3c_image_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_56018934baf3c_image_Do_Nguyen_Minh_Trang_.jpg', '2015-09-23 00:00:37', '2015-09-22 17:00:37'),
(35, 10, 'public/upload/images/product/56018935510ac_image_Do_Nguyen_Minh_Trang_.jpg', 'public/upload/images/product/thumb_56018935510ac_image_Do_Nguyen_Minh_Trang_.jpg', '2015-09-23 00:00:37', '2015-09-22 17:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_typies`
--

CREATE TABLE IF NOT EXISTS `tbl_product_typies` (
  `product_type_id` int(10) unsigned NOT NULL,
  `product_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_type_parent_id` int(10) unsigned NOT NULL,
  `product_type_fa_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product_typies`
--

INSERT INTO `tbl_product_typies` (`product_type_id`, `product_type_name`, `product_type_parent_id`, `product_type_fa_icon`, `updated_at`, `created_at`) VALUES
(32, 'Máy tính bảng', 0, 'fa fa-tablet', '2015-09-21 12:16:45', '2015-09-21 05:16:45'),
(33, 'Điện thoại', 0, 'fa fa-mobile-phone', '2015-09-21 12:20:20', '2015-09-21 05:20:20'),
(34, 'Laptop', 0, 'fa fa-laptop', '2015-09-21 12:20:42', '2015-09-21 05:20:42'),
(35, 'Máy tính bàn', 0, 'fa fa-desktop', '2015-09-21 15:30:13', '2015-09-21 05:20:55'),
(36, 'Tivi', 0, 'fa fa-tv', '2015-09-21 19:07:27', '2015-09-21 12:07:27'),
(38, 'Loài sâu bọ', 0, 'fa fa-bug', '2015-09-23 12:18:28', '2015-09-22 07:18:32'),
(39, 'Bạn gái', 0, 'fa fa-female', '2015-09-23 08:26:18', '2015-09-23 15:26:18'),
(40, 'Dell', 34, '', '2015-09-23 01:04:18', '2015-09-22 18:04:18'),
(41, 'Thú nuôi', 0, 'fa fa-paw', '2015-09-23 12:21:36', '2015-09-23 05:07:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) unsigned NOT NULL,
  `user_fullname` int(11) NOT NULL,
  `user_email` int(11) NOT NULL,
  `user_password` int(11) NOT NULL,
  `user_salt` int(11) NOT NULL,
  `user_large_avatar` int(11) NOT NULL,
  `user_small_avatar` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_admin_roles`
--
ALTER TABLE `tbl_admin_roles`
  ADD PRIMARY KEY (`admin_role_id`);

--
-- Indexes for table `tbl_home_configuration`
--
ALTER TABLE `tbl_home_configuration`
  ADD PRIMARY KEY (`home_configuration_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_histories`
--
ALTER TABLE `tbl_product_histories`
  ADD PRIMARY KEY (`product_history_id`);

--
-- Indexes for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `tbl_product_typies`
--
ALTER TABLE `tbl_product_typies`
  ADD PRIMARY KEY (`product_type_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_admin_roles`
--
ALTER TABLE `tbl_admin_roles`
  MODIFY `admin_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_home_configuration`
--
ALTER TABLE `tbl_home_configuration`
  MODIFY `home_configuration_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_product_histories`
--
ALTER TABLE `tbl_product_histories`
  MODIFY `product_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_product_typies`
--
ALTER TABLE `tbl_product_typies`
  MODIFY `product_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
