<?php
    namespace App\Http\Middleware;
    
    use Closure;
    
    class AdminLocale
    {
        public function handle($request, Closure $next)
        {
            app()->setLocale($request->cookie('admin_locale', 'en'));
            return $next($request);
        }
    }