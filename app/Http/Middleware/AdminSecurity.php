<?php

    namespace App\Http\Middleware;
    
    use Closure;
    
    use App\Http\Models\Admin;
    
    class AdminSecurity 
    {
        public function handle($request, Closure $next)
        {   
            if($request->cookie('admin_info')) {
                $this->SetSessionLastActivity($request);
                return $next($request);
            }
            
            if($request->session()->has('admin_info')) {
                $this->SetSessionLastActivity($request);
                return $next($request);
            }
            
            return redirect()->route('admin-login');
        }
        
        public function SetSessionLastActivity($request) {
            if(!$request->session()->has('last_activity')) {
                $request->session()->push(
                    'last_activity',
                    array_get(current(
                        Admin::where('admin_id', array_get(get_admin_info(), 'admin_id'))
                        ->get()
                        ->toArray()
                    ), 'admin_last_activity')
                );
            }
            return true;
        }
    }