<?php

namespace App\Http\Middleware;

use Closure;

class CheckProductRole {

    public function handle($request, Closure $next) {
        $info = get_admin_info();
        if ($info['admin_role_name'] == 'all' || $info['admin_role_name'] == 'product_manager') {
            return $next($request);
        }

        return redirect()->route('admin-not-permission');
    }

}
