<?php
    namespace App\Http\Middleware;
    
    use Closure;
    use App\Http\Models\Admin;
    
    class AdminActivity
    {
        public function handle($request, Closure $next)
        {
            if(!$request->session()->has('activity_at')) {
                Admin::where('admin_id', array_get(get_admin_info(), 'admin_id'))
                ->update(array(
                    'admin_last_activity' => date('Y-m-d H:i:s', time())
                ));
                $request->session()->push('activity_at', date('Y-m-d H:i:s', time()));
            }
            return $next($request);
            
        }
    }