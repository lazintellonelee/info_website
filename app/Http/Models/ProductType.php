<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class ProductType extends Model
    {
        protected   $table      = 'tbl_product_typies',
                    $primaryKey = 'product_type_id',
                    $fillable   = array(
                        'product_type_name',
                        'product_type_parent_id',
                        'product_type_fa_icon'
                    );
        
        public      $timestamps = true;
        
        public static function GetTypiesFull() 
        {
            $typies = ProductType::orderBy('updated_at', 'desc')->get()->toArray();
            foreach($typies as $key => $value)
            {
                if($typies[$key]['product_type_parent_id'] !== 0)
                {
                    $typies[$key]['product_type_parent_name'] = array_get(current(ProductType::where('product_type_id', $typies[$key]['product_type_parent_id'])
                                                                                             ->get()->toArray()), 'product_type_name');
                }
            }
            return $typies;
        }
        
        public static function GetTypeById($id)
        {
            $type = ProductType::where('product_type_id', $id)->first();
            if($type)
            {
                return $type->toArray();
            }
            return false;
        }
        
        public static function GetOptionHTML($data, $parent=null) {
            if(!$data) {
                return "";
            }
            $string = "";
            foreach($data as $item) {
                if(isset($item['child'])) {
                    $string .= "<optgroup label='" . ($parent!==null?$parent . ' > ':'') . $item['product_type_name'] . "'>";
                    $string .= ProductType::GetOptionHTML($item['child'], ($parent!==null?$parent . ' > ':'') . $item['product_type_name']);
                    $string .= "</optgroup>";
                    continue;
                }
                $string .= "<option value='" . $item['product_type_id'] . "'>" . $item['product_type_name'] . "</option>";
            }
            return $string;
        }
        
        public static function GetCategory($id=0) {
            $parent = ProductType::where('product_type_parent_id', $id)->get()->toArray();
            
            if(count($parent) === 0) {
                return false;
            }
            
            foreach($parent as $key => $value) {
                if(ProductType::CheckHasChild($parent[$key]['product_type_id'])) {
                    $parent[$key]['child'] = ProductType::GetCategory($parent[$key]['product_type_id']);
                }
            }
            
            return $parent;
        }
        
        public static function CheckHasChild($id) {
            if(ProductType::where('product_type_parent_id', $id)->get()->toArray()) {
                return true;
            }
            return false;
        }
        
        
    }