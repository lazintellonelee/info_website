<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;

    class ProductImage extends Model 
    {
        protected   $table      = 'tbl_product_images',
                    $primaryKey = 'product_image_id',
                    $fillable   = array(
                        'product_id',
                        'product_image_large',
                        'product_image_small',
                    );
        
        public      $timestamps = true;
        
        public static function GetGalleryProduct($product_id)
        {
            $gallery = ProductImage::where('product_id', $product_id)->get();
            if($gallery)
            {
                return $gallery->toArray();
            }
            return false;
        }
        
        public static function GetGalleryByID($product_image_id)
        {
            $product_image = ProductImage::where('product_image_id', $product_image_id)->first();
            if($product_image)
            {
                return $product_image->toArray();
            }
            return false;
        }
    }
