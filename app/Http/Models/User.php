<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected   $table      = 'tbl_users',
                $primaryKey = 'user_id',
                $fillable   = array(
                    'user_fullname',
                    'user_email',
                    'user_password',
                    'user_salt',
                    'user_large_avatar',
                    'user_small_avatar'
                );
        
    public      $timestamps = true;
    
    public static function CheckUserByEmail($email)
    {
        $user = self::where('user_email', $email)->first();
        if($user)
        {
            return $user->toArray();
        }
        return false;
    }
    
    public static function CreateUser($data)
    {
        $result = self::create($data);
        if($result)
        {
            return $result->toArray();
        }
        return false;
    }
    
    public static function GetUsers()
    {
        $users = self::all(array(
            'user_id',
            'user_fullname',
            'user_email',
            'user_small_avatar',
            'created_at'
        ));
        
        if($users)
        {
            return $users->toArray();
        }
        
        return false;
    }
}
