<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;
    use App\Http\Models\ProductType;

    class Product extends Model 
    {
        protected   $table      = 'tbl_products',
                    $primaryKey = 'product_id',
                    $fillable   = array(
                        'product_type_id',
                        'product_name',
                        'product_quantity',
                        'product_price',
                        'product_old_price',
                        'product_detail',
                        'product_label',
                        'product_promotion',
                        'product_large_image',
                        'product_small_image',
                        'created_by'
                    );
        
        public      $timestamps = true;
        
        public static function GetProducts($product_type_id=null) 
        {
            $products = Product
            ::join('tbl_admins', 'tbl_products.created_by', '=', 'tbl_admins.admin_id')
            ->orderBy('tbl_products.updated_at', 'desc')
            ->orderBy('tbl_products.created_at', 'desc')
            ->get(array(
                'tbl_admins.admin_username',
                'tbl_products.product_id',
                'tbl_products.product_type_id',
                'tbl_products.product_name',
                'tbl_products.product_large_image',
                'tbl_products.product_small_image',
                'tbl_products.product_active',
                'tbl_products.created_at'
            ));
            if($product_type_id != null)
            {
                $products = Product
                ::join('tbl_admins', 'tbl_products.created_by', '=', 'tbl_admins.admin_id')
                ->where('tbl_products.product_type_id', $product_type_id)
                ->orderBy('tbl_products.updated_at', 'desc')
                ->orderBy('tbl_products.created_at', 'desc')
                ->get(array(
                    'tbl_admins.admin_username',
                    'tbl_products.product_id',
                    'tbl_products.product_type_id',
                    'tbl_products.product_name',
                    'tbl_products.product_large_image',
                    'tbl_products.product_small_image',
                    'tbl_products.product_active',
                    'tbl_products.created_at'
                ));
            }
            
            if($products) 
            {
                $products = $products->toArray();
                foreach($products as $key => $value)
                {
                    if($products[$key]['product_type_id'] == 0)
                    {
                        $products[$key]['product_type_name'] = trans('admin-product.no-category');
                        continue;
                    }
                    
                    $type = ProductType::where('product_type_id', $products[$key]['product_type_id'])
                            ->get();
                    
                    if($type) 
                    {
                        $products[$key]['product_type_name'] = array_get(current($type->toArray()), 'product_type_name');
                        continue;
                    }
                    
                    $products[$key]['product_type_name'] = trans('admin-product.no-category');
                }
                
                return $products;
            }
            return false;
        }
        
        public static function GetProductByID($product_id)
        {
            $product = Product::where('product_id', $product_id)->first();
            if($product)
            {
                return $product->toArray();
            }
            return false;
        }
    }