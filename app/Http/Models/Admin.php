<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;

    class Admin extends Model 
    {
        protected   $table      = 'tbl_admins',
                    $primaryKey = 'admin_id',
                    $fillable   = array(
                        'admin_username',
                        'admin_password',
                        'admin_salt',
                        'admin_role_id'
                    );
        
        public      $timestamps = true;
        
        public static function GetAccounts()
        {
            $accounts = Admin
                        ::join('tbl_admin_roles', 'tbl_admin_roles.admin_role_id', '=', 'tbl_admins.admin_role_id')
                        ->where('tbl_admins.admin_id', '!=', array_get(get_admin_info(), 'admin_id'))
                        ->where('tbl_admins.admin_prefix', '!=', 'primary')
                        ->get(array(
                            'tbl_admins.admin_id',
                            'tbl_admins.admin_username',
                            'tbl_admins.admin_prefix',
                            'tbl_admins.admin_active',
                            'tbl_admins.admin_last_activity',
                            'tbl_admins.created_at',
                            'tbl_admin_roles.admin_role_title'
                        ));
            
            if($accounts)
            {
                return $accounts->toArray();
            }
            return false;
        }
          
        public static function CheckUsernameExists($admin_username)
        {
            if(Admin::where('admin_username', $admin_username)->first())
            {
                return true;
            }
            return false;
        }
    }