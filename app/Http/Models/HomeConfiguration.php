<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class HomeConfiguration extends Model
    {
        protected   $table      = 'tbl_home_configuration',
                    $primaryKey = 'home_configuration_id',
                    $fillable   = array(
                        'home_configuration_title',
                        'home_configuration_shortcut',
                        'home_configuration_logo',
                        'home_configuration_meta_description',
                        'home_configuration_meta_keywords',
                        'home_configuration_meta_author',
                        'home_configuration_refresh_state',
                        'home_configuration_refresh_second'
                    );
        
        public      $timestamps = true;
        
        public static function GetConfig()
        {
            return HomeConfiguration::where('home_configuration_id', 1)->first()->toArray();
        }
        
        public static function UpdateConfig($data)
        {
            return HomeConfiguration::where('home_configuration_id', 1)->update($data);
        }
    }
    