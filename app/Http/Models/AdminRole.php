<?php
    namespace App\Http\Models;
    
    use Illuminate\Database\Eloquent\Model;

    class AdminRole extends Model 
    {
        protected   $table      = 'tbl_admin_roles',
                    $primaryKey = 'admin_role_id';
        
        public      $timestamps = true;
        
        public static function GetAdminRoles()
        {
            $roles = AdminRole::get(array(
                'admin_role_id',
                'admin_role_name',
                'admin_role_title'
            ));
            
            if($roles)
            {
                return $roles->toArray();
            }
            return false;
        }
    }