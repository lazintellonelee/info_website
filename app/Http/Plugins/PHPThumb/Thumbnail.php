<?php
    namespace App\Http\Plugins\PHPThumb;
    
    use App\Http\Plugins\PHPThumb\GD;
    
    class Thumbnail
    {
        
        private $filename,
                $destination,
                $GD;
        
        public function __construct($filename, $destination) 
        {
            $this->destination  = $destination;
            $this->filename     = $filename;
            $this->GD           = new GD($destination . $filename);
        }
        
        public function CropAdapter()
        {
            $dimension  = $this->GD->getCurrentDimensions();
            $size       = $dimension['height'];
            if($dimension['width'] < $dimension['height']) {
                $size   = $dimension['width'];
            }
            $this->GD->cropFromCenter((int) $size);
            return $this;
        }
        
        public function Scale($size)
        {
            $this->GD->adaptiveResize($size, $size);
            return $this;
        }
        
        public function SaveWithoutThumb()
        {
            $this->GD->save($this->destination . $this->filename);
            return $this->filename;
        }
        
        public function Save()
        {
            $this->GD->save($this->destination . 'thumb_' . $this->filename);
            return 'thumb_' . $this->filename;
        }
    }
?>