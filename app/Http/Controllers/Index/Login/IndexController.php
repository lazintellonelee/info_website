<?php
namespace App\Http\Controllers\Index\Login;

use App\Http\Controllers\Controller;
 
class IndexController extends Controller
{
    public function index()
    {
        /* Get Link Login Facebook*/
        require_once './app/Http/Plugins/Facebook/autoload.php';
        session_start();
        $fb = new \Facebook\Facebook(array(
            'app_id'                => env('FB_ID'),
            'app_secret'            => env('FB_SECRET_KEY'),
            'default_graph_version' => 'v2.4',
        ));
        $helper         = $fb->getRedirectLoginHelper();
        $permissions    = array('email', 'public_profile'); // Optional permissions
        $loginUrl       = $helper->getLoginUrl(route('login-fb-callback'), $permissions);
        /* Get Link Login Facebook*/
        
        return response()->view('signup.index', array(
            'link_login_with_fb'    => htmlspecialchars($loginUrl)
        ));
    }
}