<?php
    namespace App\Http\Controllers\Admin\Index;
    
    use App\Http\Controllers\Controller;
    use App\Http\Plugins\PHPThumb\Thumbnail;
    use Illuminate\Http\Request;
    use Symfony\Component\HttpFoundation\Cookie;
    use Validator;
    
    use App\Http\Models\HomeConfiguration;
    
    class IndexController extends Controller 
    {
        public function index()
        {
            return response()->view('admin.index.index');
        }
        
        public function setting(Request $request)
        {
            $home_configuration = $this->GetConfig();
            
            if($request->isMethod('post'))
            {
                $data = $request->input();
                
                if(isset($data['home_configuration']))
                {
                    return $this->HomeConfiguration($request);
                }
                
                return redirect()->route('admin-setting')->withCookies(array(
                    new Cookie('admin_locale', $data['language'], time() + 2592000),
                    new Cookie('admin_theme', $data['theme'], time() + 2592000)
                ));
            }
            
            return response()->view('admin.index.setting', array(
                'home_config'   => $home_configuration
            ));
        }
        
        public function crawler() 
        {
            $content    =  file_get_contents('https://fortawesome.github.io/Font-Awesome/icons/');
            $container  =  array();
            $pattern    = '/<div\sclass=\"fa-hover\scol-md-3\scol-sm-4\">.+<\/div>/';
            preg_match_all($pattern, $content, $container);
            
            if(count($container[0]) == 0)
            {
                echo json_encode(array(
                    'error'   => trans('layout.edit-unsuccess')
                ));
                exit();
            }
            
            $font       = array();
            foreach($container[0] as $div)
            {
                $i_tag      = array();
                preg_match_all('/<i\sclass=\".+\"><\/i>/', $div, $i_tag);
                
                if(count($i_tag[0]) == 0)
                {
                    echo json_encode(array(
                        'error'   => trans('layout.edit-unsuccess')
                    ));
                    exit();
                }
                
                $fa_icon    = array();
                preg_match_all('/\".+\s.+\"/', $i_tag[0][0], $fa_icon);
                
                $font[] = array(
                    'name'  => str_replace('-', ' ', trim(trim($fa_icon[0][0], '"'), 'fa fa')),
                    'fa'    => trim($fa_icon[0][0], '"')
                );
            }
            
            $file = fopen("./public/json/fa-icon.json", "w");
            fwrite($file, json_encode($font));
            if(fclose($file))
            {
                echo json_encode(array(
                    'success'   => trans('layout.edit-success')
                ));
                exit();
            }
            echo json_encode(array(
                'error'   => trans('layout.edit-unsuccess')
            ));
            exit();
        }
        
        private function GetConfig()
        {
            $home_configuration = json_decode(file_get_contents(url('public/json/home-configuration.json')), true);
            if(count($home_configuration) == 0)
            {
                $home_configuration = HomeConfiguration::GetConfig();
            }
            return $home_configuration;
        }
        
        private function HomeConfiguration(Request $request)
        {   
            $validator  = Validator::make($request->all(), $this->GetRules());
            $config     = $this->GetConfig();
            
            if($validator->fails())
            {
                return response()->view('admin.index.setting', array(
                    'errors'        => $validator->errors()->all(),
                    'home_config'   => $this->GetConfig(),
                    'input'         => $request->input()
                ));
            }
            
            $data   = array(
                'home_configuration_title'                  => $request->input('home_configuration_title'),
                'home_configuration_meta_description'       => $request->input('home_meta_description'),
                'home_configuration_meta_keywords'          => $request->input('home_meta_keywords'),
                'home_configuration_meta_author'            => $request->input('home_meta_author'),
                'home_facebook'                             => $request->input('home_facebook'),
                'home_google'                               => $request->input('home_google'),
                'home_twitter'                              => $request->input('home_twitter')
            );
            
            $data['home_configuration_refresh_state'] = 0;
            if($request->input('home_configuration_refresh_state') != null)
            {
                $data['home_configuration_refresh_state'] = 1;
            }
            
            $data['home_configuration_refresh_second'] = 0;
            if($request->input('home_configuration_refresh_second') != null)
            {
                $data['home_configuration_refresh_second'] = $request->input('home_configuration_refresh_second');
            }
            
            $data['home_configuration_shortcut'] = isset($config['home_configuration_shortcut'])?$config['home_configuration_shortcut']:'';
            if($request->hasFile('home_configuration_shortcut'))
            {
                $request->file('home_configuration_shortcut')->move(
                    './public/upload/images/configuration/', 
                    'shortcut-icon.' . $request->file('home_configuration_shortcut')->getClientOriginalExtension()
                );
                $thumb = new Thumbnail(
                    'shortcut-icon.' . $request->file('home_configuration_shortcut')->getClientOriginalExtension(),
                    './public/upload/images/configuration/'
                );
                $data['home_configuration_shortcut'] = 'public/upload/images/configuration/' . $thumb->CropAdapter()->Scale(16)->SaveWithoutThumb();
            }
            
            $data['home_configuration_logo'] = isset($config['home_configuration_logo'])?$config['home_configuration_logo']:'';
            if($request->hasFile('home_configuration_logo'))
            {
                $request->file('home_configuration_logo')->move(
                    './public/upload/images/configuration/', 
                    'logo.' . $request->file('home_configuration_logo')->getClientOriginalExtension()
                );
                $data['home_configuration_logo'] = 'public/upload/images/configuration/logo.' . $request->file('home_configuration_logo')->getClientOriginalExtension();
            }
            
            if(!HomeConfiguration::UpdateConfig($data))
            {
                return response()->view('admin.index.setting', array(
                    'errors'        => array(
                        trans('layout.edit-unsuccess')
                    ),
                    'home_config'   => $this->GetConfig(),
                    'input'         => $data
                ));
            }
            
            $file = fopen("./public/json/home-configuration.json", "w");
            fwrite($file, json_encode($data));
            if(!fclose($file))
            {
                return response()->view('admin.index.setting', array(
                    'errors'        => array(
                        trans('layout.edit-unsuccess')
                    ),
                    'home_config'   => $this->GetConfig(),
                    'input'         => $data
                ));
            }
            
            $request->session()->flash('add', trans('layout.edit-success'));
            $request->session()->flash('home_configuration', 1);
            return redirect()->route('admin-setting');
        }
        
        private function GetRules() 
        {
            return array(
                'home_configuration_shortcut'   => 'image',
                'home_configuration_logo'       => 'image'
            );
        }
    }