<?php
    namespace App\Http\Controllers\Admin\ProductType;
    
    use App\Http\Controllers\Controller;
    use App\Http\Models\ProductType;
    use Validator;
    use Illuminate\Http\Request;
    use Route;
    
    class IndexController extends Controller 
    {
        public function index(Request $request)
        {
            return response()->view('admin.product-type.index', array(
                'typies'    => ProductType::GetTypiesFull()
            ));
        }
        
        public function add(Request $request)
        {
            $typies = ProductType::orderBy('product_type_name', 'asc')->get()->toArray();
            if($request->isMethod('post'))
            {
                $validator = Validator::make($request->all(), array(
                    'product_type_name'  => 'required'
                ));
                
                if($validator->fails())
                {
                    return response()->view('admin.product-type.add', array(
                        'errors'    => $validator->errors()->all(),
                        'input'     => $request->input(),
                        'typies'    => $typies
                    ));
                }
                
                if(!ProductType::create($request->input())) {
                    return response()->view('admin.product-type.add', array(
                        'errors'    => array(
                            trans('layout.add-unsuccess')
                        ),
                        'input'     => $request->input(),
                        'typies'    => $typies
                    ));
                }
                
                $request->session()->flash('add', trans('admin-product-type.add-success'));
                return redirect()->route('admin-product-type');
            }
            
            return response()->view('admin.product-type.add', array(
                'typies'    => $typies
            ));
        }
        
        public function edit(Request $request, $id=0)
        {
            $type = ProductType::GetTypeById($id);
            
            if(!$type) 
            {
                return redirect()->route('admin-product-type');
            }
            
            if($request->isMethod('post'))
            {
                $validator = Validator::make($request->all(), array(
                    'product_type_name'  => 'required'
                ));
                
                if($validator->fails())
                {
                    return response()->view('admin.product-type.add', array(
                        'type'      => $type,
                        'errors'    => $validator->errors()->all(),
                        'input'     => $request->input(),
                        'typies'    => ProductType::GetTypiesFull()
                    ));
                }
                
                if(!ProductType::where('product_type_id', $id)->update(array(
                    'product_type_name'         => $request->input('product_type_name'),
                    'product_type_parent_id'    => $request->input('product_type_parent_id'),
                    'product_type_fa_icon'      => $request->input('product_type_fa_icon')
                )))
                {
                    return response()->view('admin.product-type.edit', array(
                        'errors'    => array(
                            trans('layout.edit-unsuccess')
                        ),
                        'type'      => $type,
                        'input'     => $request->input(),
                        'typies'    => ProductType::GetTypiesFull()
                    ));
                }
                $request->session()->flash('edit', trans('admin-product-type.edit-success'));
                return redirect()->route('admin-product-type');
            }
            
            return response()->view('admin.product-type.edit', array(
                'type'      => $type,
                'typies'    => ProductType::GetTypiesFull()
            ));
        }
        
        public function delete(Request $request, $id=0)
        {
            if($request->isMethod('post')) 
            {
                if($request->input('accept_delete') !== null)
                {
                    if(!ProductType::whereIn('product_type_id', $request->input('product_type_id'))->delete())
                    {
                        return response()->view('admin.product-type.delete', array(
                            'errors'    => array(
                                trans('layout.delete-unsuccess')
                            ),
                            'typies'    => ProductType::whereIn('product_type_id', $request->input('product_type_id'))->get()->toArray()
                        ));
                    }
                    
                    $request->session()->flash('delete', trans('admin-product-type.delete-success'));
                    return redirect()->route('admin-product-type');
                } 
                
                return response()->view('admin.product-type.delete', array(
                    'typies'    => ProductType::whereIn('product_type_id', $request->input('product_type_id'))->get()->toArray()
                ));
            }
            
            return response()->view('admin.product-type.delete', array(
                'typies'    => array(
                    ProductType::GetTypeById($id)
                )
            ));
        }
    }