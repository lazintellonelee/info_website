<?php
    namespace App\Http\Controllers\Admin\User;
    
    use App\Http\Controllers\Controller;
    use App\Http\Models\User;
    use Illuminate\Http\Request;
    
    class IndexController extends Controller
    {
        public function index()
        {
            return response()->view('admin.user.index', array(
                'users' => User::GetUsers()
            ));
        }
    }