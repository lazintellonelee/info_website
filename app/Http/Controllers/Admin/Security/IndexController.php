<?php

    namespace App\Http\Controllers\Admin\Security;

    use App\Http\Controllers\Controller;
    use App\Http\Models\Admin;
    use Symfony\Component\HttpFoundation\Cookie;
    use Illuminate\Http\Request;
    use Validator;

    class IndexController extends Controller {

        public function login(Request $request) {
            if ($request->cookie('admin_info')) {
                return redirect()->route('admin-home');
            }

            if ($request->session()->has('admin_info')) {
                return redirect()->route('admin-home');
            }

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), array(
                            'username' => 'required|regex:/[a-zA-Z0-9_]*/',
                            'password' => 'required'
                ));

                if ($validator->fails()) {
                    return response()->view('admin.security.login', array(
                                'errors' => $validator->errors()->all(),
                                'input' => $request->input()
                    ));
                }

                $data = $request->input();
                $admin = Admin::where('tbl_admins.admin_username', $data['username'])
                        ->join('tbl_admin_roles', 'tbl_admin_roles.admin_role_id', '=', 'tbl_admins.admin_role_id')
                        ->get()
                        ->toArray();

                if (count($admin) === 0) {
                    return response()->view('admin.security.login', array(
                                'errors' => array(
                                    'The username is not exist.'
                                ),
                                'input' => $request->input()
                    ));
                }

                if (array_get(current($admin), 'admin_password') !== md5($data['password'] . array_get(current($admin), 'admin_salt'))) {
                    return response()->view('admin.security.login', array(
                                'errors' => array(
                                    'The password is wrong.'
                                ),
                                'input' => $request->input()
                    ));
                }

                $info = array(
                    'admin_id' => array_get(current($admin), 'admin_id'),
                    'admin_username' => array_get(current($admin), 'admin_username'),
                    'admin_role_name' => array_get(current($admin), 'admin_role_name')
                );

                $request->session()->push('admin_info', $info);

                if (isset($data['remember'])) {
                    return redirect()->route('admin-home')->withCookies(array(
                                new Cookie('admin_info', $info, time() + 2592000)
                    ));
                }
                return redirect()->route('admin-home');
            }

            return response()->view('admin.security.login');
        }

        public function logout(Request $request) {
            if ($request->session()->has('admin_info')) {
                $request->session()->clear();
            }

            if ($request->cookie('admin_info')) {
                return redirect()->route('admin-login')->withCookies(array(
                            new Cookie('admin_info', '', time() - 2592000),
                            new Cookie('admin_locale', '', time() - 2592000),
                            new Cookie('admin_theme', '', time() - 2592000)
                ));
            }

            return redirect()->route('admin-login');
        }
        
        public function notPermission()
        {
            return response()->view('admin.security.not-permission');
        }
    }
