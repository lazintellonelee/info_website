<?php
    namespace App\Http\Controllers\Admin\Product;
    
    use App\Http\Plugins\PHPThumb\Thumbnail;
    use App\Http\Models\ProductImage;
    use App\Http\Models\ProductType;
    use App\Http\Models\Product;
    use Illuminate\Http\Request;
    
    class ThumbController 
    {
        public static function CreateThumb(Request $request)
        {
            if(!$request->file('product_thumb')->isValid())
            {
                return response()->view('admin.product.add', array(
                    'errors'                => array(
                        trans('layout.upload-error')
                    ),
                    'input'                 => $request->input(),
                    'optionCategoriesHTML'  => ProductType::GetOptionHTML(ProductType::GetCategory())
                ));
            }
            
            return self::UploadImageThumbProduct($request->file('product_thumb'), $request->input('product_name'));
        }
        
        public static function UpdateThumb(Request $request, Product $product)
        {
            if(!$request->file('product_thumb')->isValid())
            {
                return response()->view('admin.product.edit', array(
                    'errors'                => array(
                        trans('layout.upload-error')
                    ),
                    'input'                 => $request->input(),
                    'optionCategoriesHTML'  => ProductType::GetOptionHTML(ProductType::GetCategory()),
                    'product'               => $product->toArray(),
                    'gallery'               => ProductImage::GetGalleryProduct($product->product_id)
                ));
            }
            return self::UploadImageThumbProduct($request->file('product_thumb'), $request->input('product_name'));
        }
        
        public static function UploadImageThumbProduct($file, $product_name)
        {
            $data = array();
            
            $image_name = uniqid() . '_' . str_replace(' ', '_', remove_vn($product_name)) . "." . $file->getClientOriginalExtension();
            $file->move('./public/upload/images/product/', $image_name);
            $data['product_large_image'] = 'public/upload/images/product/' . $image_name;

            $thumb      = new Thumbnail($image_name, './public/upload/images/product/');
            $data['product_small_image'] = 'public/upload/images/product/' . $thumb->CropAdapter()->Scale(175)->Save();
            
            return $data;
        }
        
        public static function GetThumbByProductsID($products_id)
        {
            $products = Product::whereIn('product_id', $products_id)->get();
            if($products)
            {
                $store = array();
                foreach($products->toArray() as $product)
                {
                    if($product['product_large_image'] != '')
                    {
                        $store[] = $product['product_large_image'];
                        $store[] = $product['product_small_image'];
                    }
                }
                return $store;
            }
            return array();
        }
    }