<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Product\GalleryController;
use App\Http\Controllers\Admin\Product\ThumbController;
use App\Http\Models\Product;
use Illuminate\Http\Request;

class DeleteController extends Controller 
{
    public function index(Request $request, $id = 0)
    {
        if ($request->isMethod('post')) {
            if ($request->input('accept_delete') !== null) {
                $gallery_product = GalleryController::GetGalleryInProductsID($request->input('product_id'));
                if (GalleryController::DeleteGalleryInProductsID($request->input('product_id'))) {
                    delete_store_image($gallery_product);
                    $thumb_products = ThumbController::GetThumbByProductsID($request->input('product_id'));
                    if ($this->DeleteProductByProductsID($request->input('product_id'))) {
                        delete_store_image($thumb_products);
                        $request->session()->flash('delete', trans('admin-product.delete-success'));
                        return redirect()->route('admin-product');
                    }
                }
                return response()->view('admin.product.delete', array(
                            'errors' => array(
                                trans('admin-product.delete-unsuccess')
                            ),
                            'products' => Product::whereIn('product_id', $request->input('product_id'))->get()->toArray()
                ));
            }

            return response()->view('admin.product.delete', array(
                        'products' => Product::whereIn('product_id', $request->input('product_id'))->get()->toArray()
            ));
        }

        $product = Product::GetProductByID($id);
        if ($product) {
            return response()->view('admin.product.delete', array(
                        'products' => array(
                            $product
                        )
            ));
        }
        return redirect()->route('admin-product');
    }
    
    private function DeleteProductByProductsID($products_id) {
        if (Product::whereIn('product_id', $products_id)->delete()) {
            return true;
        }
        return false;
    }
}