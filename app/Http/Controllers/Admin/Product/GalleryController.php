<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Plugins\PHPThumb\Thumbnail;
use App\Http\Models\ProductImage;
use App\Http\Models\ProductType;
use App\Http\Models\Product;
use Illuminate\Http\Request;
use Validator;

class GalleryController extends Controller 
{
    public function addGallery(Request $request, $id) {
        if ($request->hasFile('images')) {
            $product    = Product::GetProductByID($id);
            $result     = array();
            $store      = array();
            $rules      = array();
            $nbr        = count(request()->file('images')) - 1;
            foreach (range(0, $nbr) as $index) {
                $rules['images.' . $index] = 'image';
            }

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                echo json_encode(array(
                    'error' => trans('layout.upload-img-invalid')
                ));
                exit();
            }
            foreach ($request->file('images') as $file) {
                if (!$file->isValid()) {
                    delete_store_image($store);
                    echo json_encode(array(
                        'error' => trans('layout.upload-error')
                    ));
                    exit();
                }

                $data_image['product_id'] = $product['product_id'];
                $data_image = array_merge(
                        $data_image, self::UploadGalleryProduct($file, $product['product_name'])
                );

                $store[] = $data_image['product_image_large'];
                $store[] = $data_image['product_image_small'];

                $product_images = ProductImage::create($data_image);
                if (!$product_images) {
                    delete_store_image($store);
                    return response()->view('admin.product.add', array(
                                'errors' => array(
                                    trans('admin-product.add-unsuccess')
                                ),
                                'input' => $request->input(),
                                'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory())
                    ));
                }
                $result[] = $product_images->toArray();
            }

            echo json_encode($result);
            exit();
        }
    }

    public function deleteGallery(Request $request) {
        $product_image_id = $request->input('product_image_id');
        $product_image = ProductImage::GetGalleryByID($product_image_id);

        $store = array(
            $product_image['product_image_large'],
            $product_image['product_image_small']
        );
        if (!ProductImage::where('product_image_id', $product_image_id)->delete()) {
            echo json_encode(array(
                'error' => trans('layout.delete-unsuccess')
            ));
            exit();
        }
        delete_store_image($store);
        echo json_encode(array(
            'success' => 1
        ));
    }

    public static function CreateGallery(Product $product, Request $request, $store = array()) {
        foreach ($request->file('product_images') as $file) {
            if (!$file->isValid()) {
                Product::where('product_id', $product->product_id)->delete();
                delete_store_image($store);
                return response()->view('admin.product.add', array(
                            'errors' => array(
                                trans('layout.upload-error')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory())
                ));
            }

            $data_image['product_id'] = $product->product_id;
            $data_image = array_merge(
                    $data_image, self::UploadGalleryProduct($file, $product->product_name)
            );

            $store[] = $data_image['product_image_large'];
            $store[] = $data_image['product_image_small'];

            if (!ProductImage::create($data_image)) {
                ProductImage::where('product_id', $product->product_id)->delete();
                delete_store_image($store);
                return response()->view('admin.product.add', array(
                            'errors' => array(
                                trans('admin-product.add-unsuccess')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory())
                ));
            }
        }

        return true;
    }

    public static function UpdateGallery(Product $product, Request $request, $store = array()) {
        foreach ($request->file('product_images') as $file) {
            if (!$file->isValid()) {
                return response()->view('admin.product.edit', array(
                            'errors' => array(
                                trans('layout.upload-error')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory()),
                            'product' => $product->toArray(),
                            'gallery' => ProductImage::GetGalleryProduct($product->product_id)
                ));
            }

            $data_image['product_id'] = $product->product_id;
            $data_image = array_merge(
                    $data_image, self::UploadGalleryProduct($file, $request->input('product_name'))
            );

            $store[] = $data_image['product_image_large'];
            $store[] = $data_image['product_image_small'];

            if (!ProductImage::create($data_image)) {
                delete_store_image($store);
                return response()->view('admin.product.edit', array(
                            'errors' => array(
                                trans('admin-product.edit-unsuccess')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory()),
                            'product' => $product->toArray(),
                            'gallery' => ProductImage::GetGalleryProduct($product->product_id)
                ));
            }
        }

        return true;
    }

    public static function UploadGalleryProduct($file, $product_name) {
        $data_image = array();

        $image_name = uniqid() . '_image_' . str_replace(' ', '_', remove_vn($product_name)) . "." . $file->getClientOriginalExtension();
        $file->move('./public/upload/images/product/', $image_name);
        $data_image['product_image_large'] = 'public/upload/images/product/' . $image_name;
        $store[] = $data_image['product_image_large'];

        $thumb = new Thumbnail($image_name, './public/upload/images/product/');
        $data_image['product_image_small'] = 'public/upload/images/product/' . $thumb->CropAdapter()->Scale(175)->Save();
        $store[] = $data_image['product_image_small'];

        return $data_image;
    }

    public static function CheckHasGallery($product_id) {
        if (ProductImage::where($product_id)->get()) {
            return true;
        }
        return false;
    }

    public static function GetGallery($product_id) {
        $gallery = ProductImage::GetGalleryProduct($product_id);

        if (!$gallery) {
            return array();
        }

        $store = array();
        foreach ($gallery as $image) {
            $store[] = $image['product_image_large'];
            $store[] = $image['product_image_small'];
        }
        return $store;
    }

    public static function GetGalleryInProductsID($products_id) {
        $gallery = ProductImage::whereIn('product_id', $products_id)
                ->get();
        if ($gallery) {
            $store = array();
            foreach ($gallery->toArray() as $image) {
                if ($image['product_image_large'] != '') {
                    $store[] = $image['product_image_large'];
                    $store[] = $image['product_image_small'];
                }
            }

            return $store;
        }
        return array();
    }

    public static function DeleteGalleryInProductsID($products_id) {
        if (count(ProductImage::whereIn('product_id', $products_id)->get()->toArray()) > 0) {
            if (ProductImage::whereIn('product_id', $products_id)->delete()) {
                return true;
            }
            return false;
        }
        return true;
    }

}
