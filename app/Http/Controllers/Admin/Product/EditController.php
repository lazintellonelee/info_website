<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Product\GalleryController;
use App\Http\Controllers\Admin\Product\ThumbController;
use App\Http\Models\ProductImage;
use App\Http\Models\ProductType;
use App\Http\Models\Product;
use Illuminate\Http\Request;
use Validator;

class EditController extends Controller 
{
    public function index(Request $request, $id)
    {
        $product = Product::GetProductByID($id);
        if (!$product) {
            return redirect()->route('admin-product');
        }

        $gallery = ProductImage::GetGalleryProduct($id);

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), $this->GetRules());
            if ($validator->fails()) {
                return response()->view('admin.product.edit', array(
                            'errors' => $validator->errors()->all(),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory()),
                            'product' => $product,
                            'gallery' => $gallery
                ));
            }

            $data = $request->input();
            unset($data['_token']);

            if ($request->hasFile('product_images')) {
                $update_gallery = GalleryController::UpdateGallery(
                                Product::where('product_id', $id)->first(), $request, array()
                );
                if ($update_gallery != true) {
                    return $update_gallery;
                }
            }

            $store = array();
            $data['product_promotion'] = null;
            if ($request->input('product_promotion') !== null) {
                $data['product_promotion'] = $request->input('product_promotion');
            }

            if ($request->hasFile('product_thumb')) {
                $update_thumb = ThumbController::UpdateThumb($request, Product::where('product_id', $id)->first());
                if (!is_array($update_thumb)) {
                    return $update_thumb;
                }

                $data = array_merge(
                        $data, $update_thumb
                );

                $store[] = $data['product_large_image'];
                $store[] = $data['product_small_image'];
            }

            if (!Product::where('product_id', $id)->update($data)) {
                delete_store_image($store);
                return response()->view('admin.product.edit', array(
                            'errors' => array(
                                trans('admin-product.edit-unsuccess')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory()),
                            'product' => $product,
                            'gallery' => $gallery
                ));
            }
            if ($request->hasFile('product_thumb')) {
                if ($product['product_large_image'] != '') {
                    delete_store_image(array(
                        $product['product_large_image'],
                        $product['product_small_image']
                    ));
                }
            }

            $request->session()->flash('edit', trans('admin-product.edit-success'));
            return redirect()->route('admin-product');
        }

        return response()->view('admin.product.edit', array(
                    'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory()),
                    'product' => $product,
                    'gallery' => $gallery
        ));
    }
    
    private function GetRules() {
        $rules = array(
            'product_name' => 'required|max:255',
            'product_quantity' => 'required',
            'product_price' => 'required|numeric',
            'product_thumb' => 'image'
        );

        if (request()->hasFile('product_images')) {
            $nbr = count(request()->file('product_images')) - 1;
            foreach (range(0, $nbr) as $index) {
                $rules['product_images.' . $index] = 'image';
            }
        }

        return $rules;
    }
}