<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Product\GalleryController;
use App\Http\Controllers\Admin\Product\ThumbController;
use App\Http\Models\ProductType;
use App\Http\Models\Product;
use Illuminate\Http\Request;
use Validator;

class AddController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), $this->GetRules());

            if ($validator->fails()) {
                return response()->view('admin.product.add', array(
                    'errors'                => $validator->errors()->all(),
                    'input'                 => $request->input(),
                    'optionCategoriesHTML'  => ProductType::GetOptionHTML(ProductType::GetCategory())
                ));
            }

            $store = array();
            $data = $request->input();
            
            $data['created_by']         = array_get(get_admin_info(), 'admin_id');
            $data['product_promotion']  = null;
            if ($request->input('product_promotion') !== null) {
                $data['product_promotion'] = $request->input('product_promotion');
            }

            $data['product_large_image'] = '';
            $data['product_small_image'] = '';
            if ($request->hasFile('product_thumb')) {
                $thumb = ThumbController::CreateThumb($request);
                if (!is_array($thumb)) {
                    return $thumb;
                }

                $data = array_merge(
                        $data, $thumb
                );

                $store[] = $data['product_large_image'];
                $store[] = $data['product_small_image'];
            }
            
            $product = Product::create($data);
            if (!$product) {
                delete_store_image($store);
                return response()->view('admin.product.add', array(
                            'errors' => array(
                                trans('admin-product.add-unsuccess')
                            ),
                            'input' => $request->input(),
                            'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory())
                ));
            }

            if ($request->hasFile('product_images')) {
                $gallery = GalleryController::CreateGallery($product, $request, $store);
                if ($gallery != true) {
                    return $gallery;
                }
            }

            $request->session()->flash('add', trans('admin-product.add-success'));
            return redirect()->route('admin-product');
        }

        return response()->view('admin.product.add', array(
                    'optionCategoriesHTML' => ProductType::GetOptionHTML(ProductType::GetCategory())
        ));
    }
    
    private function GetRules() {
        $rules = array(
            'product_name' => 'required|max:255',
            'product_quantity' => 'required',
            'product_price' => 'required|numeric',
            'product_thumb' => 'image'
        );

        if (request()->hasFile('product_images')) {
            $nbr = count(request()->file('product_images')) - 1;
            foreach (range(0, $nbr) as $index) {
                $rules['product_images.' . $index] = 'image';
            }
        }

        return $rules;
    }
}