<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Models\ProductType;
use App\Http\Models\Product;
use Illuminate\Http\Request;

class IndexController extends Controller {

    public function index($type = 0) {
        if ($type != 0) {
            return response()->view('admin.product.index', array(
                'products'      => Product::GetProducts($type),
                'categories'    => ProductType::GetOptionHTML(ProductType::GetTypiesFull()),
                'type_id'       => $type
            ));
        }

        return response()->view('admin.product.index', array(
            'products'      => Product::GetProducts(),
            'categories'    => ProductType::GetOptionHTML(ProductType::GetCategory()),
            'type_id'       => $type
        ));
    }

    public function changeActive(Request $request) {
        if (Product::where('product_id', $request->input('product_id'))->update(array(
                    'product_active' => $request->input('product_active')
                ))) {
            echo json_encode(array(
                'success' => trans('admin-product.edit-success')
            ));
            exit();
        }
        echo json_encode(array(
            'error' => trans('admin-product.edit-unsuccess')
        ));
        exit();
    }
}
