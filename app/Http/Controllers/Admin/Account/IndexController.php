<?php
    namespace App\Http\Controllers\Admin\Account;
    
    use App\Http\Controllers\Controller;
    use App\Http\Controllers\Admin\Account\Validation;
    use App\Http\Models\Admin;
    use App\Http\Models\AdminRole;
    use Illuminate\Http\Request;
    use Validator;
    
    class IndexController extends Controller
    {
        public function index(Request $request)
        {
            return response()->view('admin.account.index', array(
                'accounts'  => Admin::GetAccounts()
            ));
        }
        
        public function add(Request $request)
        {
            if($request->isMethod('post'))
            {
                $validation = Validator::make($request->all(), $this->GetRules(), Validation::GetMessenger());
                if($validation->fails())
                {
                    return response()->view('admin.account.add', array(
                        'errors'    => $validation->errors()->all(),
                        'roles'     => AdminRole::GetAdminRoles(),
                        'input'     => $request->input()
                    ));
                }
                
                if(Admin::CheckUsernameExists($request->input('admin_username')))
                {
                    return response()->view('admin.account.add', array(
                        'errors'    => array(
                            trans('admin-account.account-exists')
                        ),
                        'roles'     => AdminRole::GetAdminRoles(),
                        'input'     => $request->input()
                    ));
                }
                
                $data                   = $request->input();
                $data['admin_salt']     = md5($data['admin_password'] . uniqid());
                $data['admin_password'] = md5($data['admin_password'] . $data['admin_salt']);
                if(!Admin::create($data))
                {
                    return response()->view('admin.account.add', array(
                        'errors'    => array(
                            trans('admin-account.add-unsuccess')
                        ),
                        'roles'     => AdminRole::GetAdminRoles(),
                        'input'     => $request->input()
                    ));
                }
                $request->session()->flash('add', trans('admin-account.add-success'));
                return redirect()->route('admin-account');
            }
            
            return response()->view('admin.account.add', array(
                'roles' => AdminRole::GetAdminRoles()
            ));
        }
        
        private function GetRules()
        {
            return array(
                'admin_username'            => 'required|regex:/^[a-zA-Z0-9_]*$/',
                'admin_password'            => 'required',
                'admin_confirm_password'    => 'same:admin_password'
            );
        }
    }