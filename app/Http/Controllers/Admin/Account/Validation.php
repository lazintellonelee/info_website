<?php
    namespace App\Http\Controllers\Admin\Account;
    
    class Validation 
    {
        public static $messenger = array(
            'vn'    => array(
                'admin_username.required'       => 'Chưa nhập tên khoản.',
                'admin_password.required'       => 'Chưa nhập mật khẩu tài khoản.',
                'admin_username.regex'          => 'Tên tài khoản không hợp lệ.',
                'admin_confirm_password.same'   => 'Xác nhận mật khẩu chưa khớp.'
            ),
            'en'    => array(
                'admin_username.required'       => 'The account name field is required.',
                'admin_password.required'       => 'The password field is required.',
                'admin_username.regex'          => 'The account name format is invalid.',
                'admin_confirm_password.same'   => 'The password confirmation does not match.'
            )
        );
        
        public static function GetMessenger()
        {
            return array_get(Validation::$messenger, app()->getLocale());
        }
    }