<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;

class IndexController extends Controller 
{
    public function index()
    {
        return response()->view('admin.news-category.index');
    }
}