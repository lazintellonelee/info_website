<?php

namespace App\Http\Controllers\Facebook;


use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller 
{
    public function index(Request $rq)
    {
        require_once './app/Http/Plugins/Facebook/autoload.php';
        session_start();
        $fb = new \Facebook\Facebook([
            'app_id'                => env('FB_ID'),
            'app_secret'            => env('FB_SECRET_KEY'),
            'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // Logged in
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client   = $fb->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata  = $oAuth2Client->debugToken($accessToken);
        $fbApp          = new \Facebook\FacebookApp(env('FB_ID'), env('FB_SECRET_KEY'));
        $request        = new \Facebook\FacebookRequest($fbApp, $accessToken->getValue(), 'GET', '/me?fields=id,name,picture,email');
        
        try {
            $response = $fb->getClient()->sendRequest($request);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode  = $response->getGraphNode()->asArray();
        $data       = array();
        $user       = User::CheckUserByEmail($graphNode['email']);
        
        if($user)
        {
            $data['user_id']            = $user['user_id'];
            $data['user_fullname']      = $user['user_fullname'];
            $data['user_email']         = $user['user_email'];
            $data['user_small_avatar']  = $graphNode['picture']['url'];
            
            if($graphNode['picture']['url'] != $user['user_small_avatar'])
            {
                User::where('user_email', $user['user_email'])->update(array(
                    'user_small_avatar' => $graphNode['picture']['url']
                ));
            }
        }
        else {
            $user = User::CreateUser(array(
                'user_fullname'     => $graphNode['name'],
                'user_email'        => $graphNode['email'],
                'user_small_avatar' => $graphNode['picture']['url'],
                'user_password'     => null,
                'user_salt'         => null
            ));
            
            $data['user_id']            = $user['user_id'];
            $data['user_fullname']      = $user['user_fullname'];
            $data['user_email']         = $user['user_email'];
            $data['user_small_avatar']  = $graphNode['picture']['url'];
        }
        
        $rq->session()->push('user_info', $data);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId(env('FB_ID'));
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }

            echo '<h3>Long-lived</h3>';
            var_dump($accessToken->getValue());
        }

        $_SESSION['fb_access_token'] = (string) $accessToken;
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        return redirect('/');
    }

}
