<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*ADMIN LOGIN*/
Route::match(array('post', 'get'), 'admin/login', array('as' => 'admin-login', 'uses' => 'Admin\Security\IndexController@login'));
/*ADMIN LOGIN*/

Route::group(array(
    'middleware'    => array(
        'admin.security',
        'admin.activity',
        'admin.locale'
    ),
    'prefix'        => 'admin'
), function() {
    /*ADMIN LOGOUT*/
    Route::get('/logout', array(
        'as'    => 'admin-logout',
        'uses'  => 'Admin\Security\IndexController@logout'
    ));
    /*ADMIN LOGOUT*/
    
    /*NOT PERMISSION*/
    Route::get('/not-permission', array(
        'as'    => 'admin-not-permission',
        'uses'  => 'Admin\Security\IndexController@notPermission'
    ));
    /*NOT PERMISSION*/
    
    /*CRAWLER FONT AWSOME*/
    Route::post('/crawler', array(
        'as'    => 'admin-crawler-fa-icon',
        'uses'  => 'Admin\Index\IndexController@crawler'
    ));
    /*CRAWLER FONT AWSOME*/
    
    Route::match(array(
        'get',
        'post'
    ), '/setting', array(
        'as'    => 'admin-setting',
        'uses'  => 'Admin\Index\IndexController@setting'
    ));
    
	Route::get('/', array(
        'as'    => 'admin-home',
        'uses'  => 'Admin\Index\IndexController@index'
    ));
    
    /*ADMIN PRODUCT*/
    Route::group(array(
        'middleware'    => array(
            'admin.role.product'
        ),
        'prefix'        => 'product'
    ), function(){
        Route::get('{type?}', array(
            'as'    => 'admin-product',
            'uses'  => 'Admin\Product\IndexController@index'
        ))->where('type', '[0-9]+');

        Route::match(array(
            'get',
            'post'
        ), 'add', array(
            'as'    => 'admin-product-add',
            'uses'  => 'Admin\Product\AddController@index'
        ));

        Route::match(array(
            'get',
            'post'
        ), 'edit/{id}', array(
            'as'    => 'admin-product-edit',
            'uses'  => 'Admin\Product\EditController@index'
        ))->where('id', '[0-9]+');

        Route::post('add-gallery/{id}', array(
            'as'    => 'admin-product-add-gallery',
            'uses'  => 'Admin\Product\GalleryController@addGallery'
        ))->where('id', '[0-9]+');

        Route::post('delete-gallery', array(
            'as'    => 'admin-product-delete-gallery',
            'uses'  => 'Admin\Product\GalleryController@deleteGallery'
        ));

        Route::post('change-active', array(
            'as'    => 'admin-product-change-active',
            'uses'  => 'Admin\Product\IndexController@changeActive'
        ));

        Route::match(array(
            'get',
            'post'
        ), 'delete/{id?}', array(
            'as'    => 'admin-product-delete',
            'uses'  => 'Admin\Product\DeleteController@index'
        ))->where('id', '[0-9]+');
    });
    /*ADMIN PRODUCT*/
    
    /*ADMIN PRODUCT TYPE*/
    Route::group(array(
        'middleware' => array(
            'admin.role.product'
        ), 
        'prefix'    => 'product-type'
    ), function() {
        Route::get('/', array(
            'as'    => 'admin-product-type',
            'uses'  => 'Admin\ProductType\IndexController@index'
        ));

        Route::match(array(
            'get',
            'post'
        ), '/add', array(
            'as'    => 'admin-product-type-add',
            'uses'  => 'Admin\ProductType\IndexController@add'
        ));

        Route::match(array(
            'get',
            'post'
        ), '/edit/{id}', array(
            'as'    => 'admin-product-type-edit',
            'uses'  => 'Admin\ProductType\IndexController@edit'
        ))->where('id', '[0-9]+');

        Route::match(array(
            'get',
            'post'
        ), '/delete/{id?}', array(
            'as'    => 'admin-product-type-delete',
            'uses'  => 'Admin\ProductType\IndexController@delete'
        ))->where('id', '[0-9]+');
    });
    /*ADMIN PRODUCT TYPE*/
    
    /*ADMIN NEWS CATEGORY*/
    Route::group(array(
        'middleware'    => array(
            'admin.role.news'
        ),
        'prefix'        => 'news-category'
    ), function(){
        Route::get('/', array(
            'as'    => 'admin-news-category',
            'uses'  => 'Admin\NewsCategory\IndexController@index'
        ));
    });
    /*ADMIN NEWS CATEGORY*/
    
    /*ADMIN ACCOUNT*/
    Route::group(array(
        'middleware'    => array(
            'admin.role.account'
        ),
        'prefix'        => 'account'
    ), function(){
        Route::get('/', array(
            'as'    => 'admin-account',
            'uses'  => 'Admin\Account\IndexController@index'
        ));
        Route::match(array(
            'post',
            'get'
        ), '/add', array(
            'as'    => 'admin-account-add',
            'uses'  => 'Admin\Account\IndexController@add'
        ));
    });
    /*ADMIN ACCOUNT*/
    
    /*ADMIN USER*/
    Route::group(array(
        'middleware'    => array(
            
        ),
        'prefix'        => 'user'
    ), function(){
        Route::get('/', array(
            'as'    => 'admin-user',
            'uses'  => 'Admin\User\IndexController@index'
        ));
    });
    /*ADMIN USER*/
});

//SIGN IN AND UP
Route::get('/signin', array(
    'as'    => 'signup',
    'uses'  => 'Index\Login\IndexController@index'
));
//SIGN IN AND UP

//FOR FACEBOOK LOGIN
Route::get('/login_fb_callback', array(
    'as'    => 'login-fb-callback',
    'uses'  => 'Facebook\IndexController@index'
));
//FOR FACEBOOK

