<?php
    return array(
        'title-add'         => 'Add new account',
        'list-account'      => 'List account',
        'prefix'            => 'Prefix',
        'username'          => 'Account name',
        'password'          => 'Password',
        'confirm-password'  => 'Confirm password',
        'prefix'            => 'Prefix',
        'created'           => 'Created at',
        'last-active'       => 'Last active',
        'check-all-delete'  => 'Delete account has checked',
        'add'               => 'Add new account',
        'account-exists'    => 'The account name already exists.',
        'add-unsuccess'     => 'Add account failed! Please try again.',
        'add-success'       => 'Add account success'
    );