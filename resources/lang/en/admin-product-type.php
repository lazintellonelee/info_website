<?php
    return array(
        'title'         => 'List product category',
        'th-name'       => 'Name',
        'th-parent'     => 'Parent name',
        'th-created'    => 'Created at',
        'btn-add'       => 'Add product category',
        'btn-delete'    => 'Delete product category',
        'title-edit'    => 'Edit product category',
        'title-delete'  => 'Delete product category',
        'is-parent'     => 'Is Parent',
        
        'add-success'       => 'Add product category success.',
        'edit-success'      => 'Edit product category success.',
        'delete-success'    => 'Delete product category success',
        'check-all-delete'  => 'Delete product category has checked',
        'choose-category'   => 'Choose category',
        
        'delete-label'      => 'Do you want to delete the following product categories'
    );