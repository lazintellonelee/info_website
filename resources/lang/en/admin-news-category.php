<?php

return array(
    'list-news-category'    => 'List news category',
    'th-name'               => 'Name',
    'th-parent'             => 'Parent',
    'th-created'            => 'Created',
    
    'title-add'             => 'Add new category'
);