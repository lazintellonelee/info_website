<?php

return array(
    'list-user'     => 'List User',
    'th-fullname'   => 'Fullname',
    'th-email'      => 'Email',
    'th-created-at' => 'Created at',
);