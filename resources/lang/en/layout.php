<?php
    return array(
        'logout-btn'            => 'Logout',
        'last-activity'         => 'Last login at',
        'dashboard'             => 'Dashboard',
        'product-management'    => 'Product Management',
        'news-management'       => 'News Management',
        'user-management'       => 'User Management',
        'list-product'          => 'List Product',
        'list-product-type'     => 'List Product Category',
        'list-news'             => 'List News',
        'list-news-category'    => 'List News Category',
        'list-news-vote'        => 'List News Vote',
        'administrator'         => 'Administrators',
        'setting'               => 'Setting',
        'view-website'          => 'View Website',
        'btn-save'              => 'Save',
        'btn-edit'              => 'Edit',
        'btn-update'            => 'Update',
        'btn-delete'            => 'Delete',
        'btn-back'              => 'Back',
        'btn-uploading'         => 'Uploading',
        'cancel'                => 'Cancel',
        
        'theme'                 => 'Theme',
        'language'              => 'Language',
        
        
        'add-unsuccess'         => 'Add failed! Please try again.',
        'edit-unsuccess'        => 'Update failed! Please try again.',
        'edit-success'          => 'Update success.',
        'delete-unsuccess'      => 'Delete failed! Please try again.',
        'upload-error'          => 'There was an error occurred during the upload process.',
        'upload-img-invalid'    => 'File upload must be image.',
        
        'website-configuration' => 'Website configuration',
        'profile-configuration' => 'Profile configuration',
        'admin-configuration'   => 'Admin configuration',
        'home-configuration'    => 'Home configuration',
        
        'home-config-title'     => 'Title',
        'home-config-shortcut'  => 'Shortcut Icon',
        'home-config-logo'      => 'Logo',
        
        'home-config-meta-description'  => 'Description',
        'home-config-meta-keywords'     => 'Keywords',
        'home-config-meta-author'       => 'Author',
        'home-config-meta-refresh'      => 'Refresh',
        
        'social-network'                => 'Social website',
        'all'                           => 'All',
        'no-data'                       => 'No data',
        'search'                        => 'Search',
        
        'warning'               => 'Warning',
        'not-permission-title'  => 'Not permission',
        'not-permission-text'   => 'You are not authorized to access that function. Please contact to the administrator to know more or upgrade permission.'
    );