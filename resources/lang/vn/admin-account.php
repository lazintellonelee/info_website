<?php
    return array(
        'title-add'         => 'Tạo tài khoản quản trị',
        'list-account'      => 'Danh sách tài khoản',
        'prefix'            => 'Chức danh',
        'username'          => 'Tên tài khoản',
        'password'          => 'Mật khẩu',
        'confirm-password'  => 'Xác nhận mật khẩu',
        'prefix'            => 'Chức danh',
        'created'           => 'Tạo lúc',
        'last-active'       => 'Lần cuối đăng nhập',
        'check-all-delete'  => 'Xoá tại khoản được chọn',
        'add'               => 'Tạo tài khoản',
        'account-exists'    => 'Tên tài khoản đã tồn tại.',
        'add-unsuccess'     => 'Tạo tài khoản không thành công! Vui lòng thử lại',
        'add-success'       => 'Tạo tài khoản thành công.'
    );
