<?php
    return array(
        'title'         => 'Danh mục sản phẩm',
        'th-name'       => 'Tên danh mục',
        'th-parent'     => 'Danh mục cha',
        'th-created'    => 'Tạo lúc',
        'btn-add'       => 'Thêm danh mục',
        'btn-delete'    => 'Xoá danh mục',
        'title-edit'    => 'Sửa danh mục',
        'title-delete'  => 'Xoá danh mục',
        'is-parent'     => 'Danh mục cha',
        
        'add-success'       => 'Thêm danh mục sản phẩm thành công.',
        'edit-success'      => 'Cập nhật danh mục sản phẩm thành công.',
        'delete-success'    => 'Xoá danh mục thành công.',
        'check-all-delete'  => 'Xoá danh mục sản phẩm được chọn',
        'choose-category'   => 'Chọn danh mục',
        
        'delete-label'      => 'Bạn muốn xoá những danh mục sau'
    );