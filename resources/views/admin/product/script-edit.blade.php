<script type="text/javascript">
    $(document).ready(function(){
        <?php if(isset($input['product_type_id']) && ($input['product_type_id'] != '0' || $input['product_type_id'] != '')):?>
        $('#product_type_id').val('<?= $input['product_type_id']?>');
        <?php else:?>
        $('#product_type_id').val('<?= $product['product_type_id']?>');
        <?php endif;?>
        
        $('#product_type_id').chosen({
            width                           : '100%',
            height                          : '30px',
            placeholder_text_single         : 'Choose A category',
            include_group_label_in_selected : true
        });
        
        CKEDITOR.replace('product_detail', {
            removePlugins   : 'elementspath',
            entities_latin  : false,
            enterMode       : CKEDITOR.ENTER_BR,
            height          : 275,
            toolbar: [
                {name: 'format', items: ['Styles','Format','Font','FontSize']},
                '/',
                {name: 'clipboard', items: ['Source', 'Undo', 'Redo', '-', 'Find', '-', 'Replace', '-', 'Bold', '-', 'Italic', '-', 'Underline', '-', 'Strike', '-', 'Subscript', '-', 'Superscript', '-', 'RemoveFormat', '-', 'TextColor', '-', 'BGColor', '-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', '-', 'Image', '-', 'Maximize']}
            ]
        });
        
        CKEDITOR.config.filebrowserBrowseUrl        = "<?= url('public/plugins/ckfinder/ckfinder.html')?>";
        CKEDITOR.config.filebrowserImageBrowseUrl   = "<?= url('public/plugins/ckfinder/ckfinder.html?type=Images')?>";
        CKEDITOR.config.filebrowserFlashBrowseUrl   = "<?= url('public/plugins/ckfinder/ckfinder.html?type=Flash')?>";
        CKEDITOR.config.filebrowserUploadUrl        = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')?>";
        CKEDITOR.config.filebrowserImageUploadUrl   = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')?>";
        CKEDITOR.config.filebrowserFlashUploadUrl   = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')?>";
        
        $(document).on('change', '#product_label', function(event) {
            if($(this).val() === 'promotion') 
            {
                $('#product_promotion').attr('disabled', false);
                return true;
            }
            $('#product_promotion').val('');
            $('#product_promotion').attr('disabled', true);
            return true;
        });
        
        <?php if(isset($input['product_label'])):?>
        $('#product_label').val('<?= $input['product_label']?>');
        <?php else:?>
        $('#product_label').val('<?= $product['product_label']?>');
        <?php endif;?>
        
        <?php if(isset($input['product_promotion'])):?>
        $('#product_promotion').attr('disabled', false);
        <?php elseif($product['product_label'] === 'promotion'):?>
        $('#product_promotion').attr('disabled', false);
        <?php endif;?>
            
        $('.image-popup').magnificPopup({
            type    : 'image'
        });
        
        $('.gallery').magnificPopup({
            gallery : {
                enabled: true
            },
            type    :'image'
        });
        
        /*FOR DRAG UPLOAD IMAGE PRODUCT*/
        document.getElementById('gallery_container').addEventListener('dragover' , function(event){
            event.preventDefault();
        }, false);
        
        document.getElementById('gallery_container').addEventListener('drop', function(event){
            event.preventDefault();
            $('#gallery_container i.empty-gallery').remove();
            
            var files   = event.dataTransfer.files;
            var http    = new XMLHttpRequest();
            var form    = new FormData();
            form.append('_token', $('meta[name="csrf-token"]').attr('content'));
            for(var index=0; index<files.length; index++)
            {
                form.append('images[]', files[index]);
                var html = '<div class="col-sm-2 col-xs-4 no-padding uploading-container">\n\
                            <div class="col-sm-12 col-xs-12">\n\
                            <img class="img-responsive" src="<?= url('public/img/uploading.gif')?>" />\n\
                            </div>\n\
                            <div class="col-sm-12 col-xs-12 margin-5">\n\
                            <button type="button" class="btn btn-default btn-sm btn-full-size btn-no-radius text-center delete-gallery" disabled><?= trans('layout.btn-uploading')?> ...</button>\n\
                            </div>\n\
                            </div>';
                $('#gallery_container').append(html);
            }
            
            http.open('POST', '<?= route('admin-product-add-gallery', array('id' => $product['product_id']))?>', true);
            http.onload = function(event)
            {
                $('#gallery_container .uploading-container').remove();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    if($('#gallery_container .image-container').length === 0)
                    {
                        $('#gallery_container').append('<i class="margin-20 empty-gallery"><?= trans('admin-product.no-gallery')?></i>');
                    }
                    CallNoty('alert', result.error);
                    return true;
                }
                
                for(var i=0; i<result.length; i++) 
                {
                    var html = '<div class="col-sm-2 col-xs-4 no-padding image-container">\n\
                                <div class="col-sm-12 col-xs-12">\n\
                                <a href="<?= url()?>/' + result[i].product_image_large + '" class="gallery">\n\
                                <img class="img-responsive" src="<?= url()?>/' + result[i].product_image_small + '" />\n\
                                </a>\n\
                                </div>\n\
                                <div class="col-sm-12 col-xs-12 margin-5 delete-gallery-container">\n\
                                <div class="col-sm-12 col-xs-12 no-padding delete-gallery-confirm-container hidden">\n\
                                <button product-image-id="' + result[i].product_image_id + '" type="button" class="btn btn-success btn-sm btn-no-radius text-center pull-left delete-gallery-yes"><i class="fa fa-fw fa-check"></i></button>\n\
                                <button type="button" class="btn btn-danger btn-sm btn-no-radius text-center pull-right delete-gallery-no"><i class="fa fa-fw fa-times"></i></button>\n\
                                </div>\n\
                                <div class="col-sm-12 col-xs-12 no-padding delete-gallery-btn-container">\n\
                                <button product-image-id="' + result[i].product_image_id + '" type="button" class="btn btn-default btn-sm btn-full-size btn-no-radius text-center delete-gallery"><?= trans('layout.btn-delete')?></button>\n\
                                </div>\n\
                                </div>';
                    $('#gallery_container').append(html);
                }
                $('.gallery').magnificPopup({
                    gallery : {
                        enabled: true
                    },
                    type    :'image'
                });
                return true;
            };
            http.send(form);
        }, false);
        /*FOR DRAG UPLOAD IMAGE PRODUCT*/
        
        /*FOR DELETE GALLERY*/
        $(document).on('click', '.delete-gallery', function(event){
            var _this = this;
            $(_this).parent('.delete-gallery-btn-container').addClass('hidden')
                    .parent('.delete-gallery-container').children('.delete-gallery-confirm-container')
                    .removeClass('hidden');
        });
        
        $(document).on('click', '.delete-gallery-yes', function(event){
            var _this               = this;
            var product_image_id    = $(this).attr('product-image-id');
            var form                = new FormData();
            var http                = new XMLHttpRequest();
            form.append('product_image_id', product_image_id);
            form.append('_token', $('meta[name="csrf-token"]').attr('content'));
            http.open('POST', '<?= url()?>/admin/product/delete-gallery', true);
            http.onload = function(event)
            {
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    CallNoty('error', result.error);
                    return true;
                }
                $(_this).parents('.image-container').remove();
                CallNoty('success', '<?= trans('admin-product.delete-gallery-success')?>');
                if($('#gallery_container .image-container').length === 0)
                {
                    $('#gallery_container').append('<i class="margin-20 empty-gallery"><?= trans('admin-product.no-gallery')?></i>');
                    return true;
                }
                $('.gallery').magnificPopup({
                    gallery : {
                        enabled: true
                    },
                    type    :'image'
                });
            };
            http.send(form);
        });
        
        $(document).on('click', '.delete-gallery-no', function(event){
            $(this).parent('.delete-gallery-confirm-container').addClass('hidden')
                   .parent('.delete-gallery-container').children('.delete-gallery-btn-container')
                   .removeClass('hidden');
        });
        /*FOR DELETE GALLERY*/
    });
</script>