@extends('layout.admin')

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/chosen/chosen.jquery.js') }}"></script>
@endsection

@section('head_link')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen-bootstrap.css') }}" />
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('layout.btn-back') }}" href="{{ get_previous_link() }}">
            <i class="fa fa-fw fa-chevron-circle-left"></i>
        </a>
        {{ trans('admin-product.title-add') }}
    </legend>
    <form method="post" enctype="multipart/form-data">
        <div class="col-sm-12 col-xs-12 margin-5">
            @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        
        {!! csrf_field() !!}
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.th-category') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-6">
                <select class="form-control input-sm" id="product_type_id" name="product_type_id" data-placeholder="{{ trans('admin-product-type.choose-category') }} ...">
                    <option value=""></option>
                    <?= $optionCategoriesHTML?>
                </select>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.th-name') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-6">
                <input type="text" class="form-control input-sm" name="product_name" value="{{ $input['product_name'] or '' }}" required />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-quantity') }}</b></div>
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-price') }}</b></div>
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-old-price') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_quantity" value="{{ $input['product_quantity'] or '' }}" required />
            </div>
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_price" value="{{ $input['product_price'] or '' }}" required />
            </div>
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_old_price" value="{{ $input['product_old_price'] or '' }}"  />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.title-detail') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-6">
                <textarea class="form-control input-sm" name="product_detail" id="product_detail">{{ $input['product_detail'] or '' }}</textarea>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-label') }}</b></div>
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-promotion') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6">
                <select class="form-control input-sm" name="product_label" id="product_label">
                    <option value="new">{{ trans('admin-product.title-label-new') }}</option>
                    <option value="hot">{{ trans('admin-product.title-label-hot') }}</option>
                    <option value="promotion">{{ trans('admin-product.title-label-promotion') }}</option>
                </select>
            </div>
            <div class="col-sm-6 col-xs-6">
                <input type="number" class="form-control input-sm" disabled="true" name="product_promotion" id="product_promotion" value="{{ $input['product_promotion'] or '' }}" />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-thumb') }}</b></div>
            @if(env('PRODUCT_GALLERY', false))
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-images') }}</b></div>
            @endif
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6">
                <input type="file" class="form-control input-sm" name="product_thumb" />
            </div>
            @if(env('PRODUCT_GALLERY', false))
            <div class="col-sm-6 col-xs-6">
                <input type="file" multiple class="form-control input-sm" name="product_images[]" />
            </div>
            @endif
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-20 text-right">
            <a href="<?= route('admin-product')?>" class="btn btn-default btn-no-radius">{{ trans('layout.cancel') }}</a>
            <button type="submit" class="btn btn-primary btn-no-radius">{{ trans('admin-product.title-add') }}</button>
        </div>
    </form>
    <legend></legend>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        <?php if(isset($input['product_type_id']) && ($input['product_type_id'] != '0' || $input['product_type_id'] != '')):?>
        $('#product_type_id').val('<?= $input['product_type_id']?>');
        <?php endif;?>
        
        $('#product_type_id').chosen({
            width                           : '100%',
            height                          : '30px',
            placeholder_text_single         : 'Choose A category',
            include_group_label_in_selected : true
        });
        
        CKEDITOR.replace('product_detail', {
            removePlugins   : 'elementspath',
            entities_latin  : false,
            enterMode       : CKEDITOR.ENTER_BR,
            height          : 275,
            toolbar: [
                {name: 'format', items: ['Styles','Format','Font','FontSize']},
                '/',
                {name: 'clipboard', items: ['Source', 'Undo', 'Redo', '-', 'Find', '-', 'Replace', '-', 'Bold', '-', 'Italic', '-', 'Underline', '-', 'Strike', '-', 'Subscript', '-', 'Superscript', '-', 'RemoveFormat', '-', 'TextColor', '-', 'BGColor', '-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', '-', 'Image', '-', 'Maximize']}
            ]
        });
        
        CKEDITOR.config.filebrowserBrowseUrl        = "<?= url('public/plugins/ckfinder/ckfinder.html')?>";
        CKEDITOR.config.filebrowserImageBrowseUrl   = "<?= url('public/plugins/ckfinder/ckfinder.html?type=Images')?>";
        CKEDITOR.config.filebrowserFlashBrowseUrl   = "<?= url('public/plugins/ckfinder/ckfinder.html?type=Flash')?>";
        CKEDITOR.config.filebrowserUploadUrl        = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')?>";
        CKEDITOR.config.filebrowserImageUploadUrl   = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')?>";
        CKEDITOR.config.filebrowserFlashUploadUrl   = "<?= url('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')?>";
        
        $(document).on('change', '#product_label', function(event) {
            if($(this).val() === 'promotion') 
            {
                $('#product_promotion').attr('disabled', false);
                return true;
            }
            $('#product_promotion').val('');
            $('#product_promotion').attr('disabled', true);
            return true;
        });
        
        <?php if(isset($input['product_label'])):?>
        $('#product_label').val('<?= $input['product_label']?>');
        <?php endif;?>
        
        <?php if(isset($input['product_promotion'])):?>
        $('#product_promotion').attr('disabled', false);
        <?php endif;?>
    });
</script>
@endsection