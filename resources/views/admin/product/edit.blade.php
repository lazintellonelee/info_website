@extends('layout.admin')

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/chosen/chosen.jquery.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
@endsection

@section('head_link')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen-bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/magnific-popup/dist/magnific-popup.css') }}" />
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('layout.btn-back') }}" href="{{ get_previous_link() }}">
            <i class="fa fa-fw fa-chevron-circle-left"></i>
        </a>
        {{ trans('admin-product.title-edit') }}
    </legend>
    <form method="post" enctype="multipart/form-data">
        <div class="col-sm-12 col-xs-12 margin-5">
            @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        
        {!! csrf_field() !!}
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.th-category') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-6">
                <select class="form-control input-sm" id="product_type_id" name="product_type_id" data-placeholder="{{ trans('admin-product-type.choose-category') }} ...">
                    <option value=""></option>
                    <?= $optionCategoriesHTML?>
                </select>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.th-name') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-6">
                <input type="text" class="form-control input-sm" name="product_name" value="{{ $input['product_name'] or $product['product_name'] }}" required />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-quantity') }}</b></div>
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-price') }}</b></div>
            <div class="col-sm-4 col-xs-4"><b>{{ trans('admin-product.title-old-price') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_quantity" value="{{ $input['product_quantity'] or $product['product_quantity'] }}" required />
            </div>
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_price" id="product_price" value="{{ $input['product_price'] or $product['product_price'] }}" required />
            </div>
            <div class="col-sm-4 col-xs-4">
                <input type="number" class="form-control input-sm" name="product_old_price" id="product_old_price" value="{{ $input['product_old_price'] or $product['product_old_price'] }}"  />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-12 col-xs-6"><b>{{ trans('admin-product.title-detail') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-12 col-xs-12">
                <textarea class="form-control input-sm" name="product_detail" id="product_detail">{{ $input['product_detail'] or $product['product_detail'] }}</textarea>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-label') }}</b></div>
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-promotion') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6">
                <select class="form-control input-sm" name="product_label" id="product_label">
                    <option value="new">{{ trans('admin-product.title-label-new') }}</option>
                    <option value="hot">{{ trans('admin-product.title-label-hot') }}</option>
                    <option value="promotion">{{ trans('admin-product.title-label-promotion') }}</option>
                </select>
            </div>
            <div class="col-sm-6 col-xs-6">
                <input type="number" class="form-control input-sm" disabled="true" name="product_promotion" id="product_promotion" value="{{ $input['product_promotion'] or $product['product_promotion'] }}" />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-10">
            <div class="col-sm-6 col-xs-6">
                <b>{{ trans('admin-product.title-thumb') }}</b>
                @if($product['product_large_image'] !== '')
                <a href="{{ url($product['product_large_image']) }}" class="image-popup pull-right">
                    <i class="fa fa-fw fa-image"></i> {{ trans('admin-product.view-thumb') }}
                </a>
                @endif
            </div>
            @if(env('PRODUCT_GALLERY', false))
            <div class="col-sm-6 col-xs-6"><b>{{ trans('admin-product.title-images') }}</b></div>
            @endif
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6">
                <input type="file" class="form-control input-sm" name="product_thumb" />
            </div>
            @if(env('PRODUCT_GALLERY', false))
            <div class="col-sm-6 col-xs-6">
                <input type="file" multiple class="form-control input-sm" name="product_images[]" />
            </div>
            @endif
        </div>
        @if(env('PRODUCT_GALLERY', false))
        <div class="col-sm-12 col-xs-12">
            <div class="col-sm-12 col-xs-12 margin-20 no-padding gallery-container" id="gallery_container">
                <legend>{{ trans('admin-product.title-images') }}</legend>
                @if($gallery)
                    @foreach($gallery as $image)
                    <div class="col-sm-2 col-xs-4 no-padding image-container">
                        <div class="col-sm-12 col-xs-12">
                            <a href="{{ url($image['product_image_large']) }}" class="gallery">
                                <img class="img-responsive" src="{{ url($image['product_image_small']) }}" />
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-12 margin-5 delete-gallery-container">
                            <div class="col-sm-12 col-xs-12 no-padding delete-gallery-confirm-container hidden">
                                <button product-image-id="{{ $image['product_image_id'] }}" type="button" class="btn btn-success btn-sm btn-no-radius text-center pull-left delete-gallery-yes"><i class="fa fa-fw fa-check"></i></button>
                                <button type="button" class="btn btn-danger btn-sm btn-no-radius text-center pull-right delete-gallery-no"><i class="fa fa-fw fa-times"></i></button>
                            </div>
                            <div class="col-sm-12 col-xs-12 no-padding delete-gallery-btn-container">
                                <button product-image-id="{{ $image['product_image_id'] }}" type="button" class="btn btn-default btn-sm btn-full-size btn-no-radius text-center delete-gallery">{{ trans('layout.btn-delete') }}</button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                <i class="margin-20 empty-gallery">{{ trans('admin-product.no-gallery') }}</i>
                @endif
            </div>
            <legend></legend>
        </div>
        @endif
        
        <div class="col-sm-12 col-xs-12 margin-20 text-right">
            <a href="<?= route('admin-product')?>" class="btn btn-default btn-no-radius">{{ trans('layout.cancel') }}</a>
            <button type="submit" class="btn btn-primary btn-no-radius">{{ trans('admin-product.title-edit') }}</button>
        </div>
    </form>
    <legend></legend>
</div>

@include('admin.product.script-edit', array('input' => isset($input)?$input:null, 'product' => $product))

@endsection