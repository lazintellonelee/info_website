@extends('layout.admin')

@section('head_link')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/datatable/css/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen-bootstrap.css') }}" />
@endsection

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/datatable/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/javascript-template/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/chosen/chosen.jquery.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('admin-account.title-add') }}"  href="{{ route('admin-account-add') }}"><i class="fa fa-fw fa-plus-circle"></i></a>
        {{ trans('admin-account.list-account') }}
    </legend>
    
    @foreach(array('add','edit','delete') as $item)
        @if(Session::has($item))
            <div class="col-sm-12 col-xs-12 no-padding margin-5">
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get($item) }}
                </div>
            </div>
        @endif
    @endforeach
    
    <div id="container_form_delete" class="hidden"></div>
    
    <table class="table table-hover table-responsive" id="table-admins">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>{{ trans('admin-account.username') }}</th>
                <th>{{ trans('admin-account.prefix') }}</th>
                <th>{{ trans('admin-account.last-active') }}</th>
                <th>{{ trans('admin-account.created') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($accounts)
            @foreach($accounts as $account)
            <tr>
                <td>
                    <input type="checkbox" class="check-admin" admin-id="{{ $account['admin_id'] }}" value="{{ $account['admin_id'] }}" />
                </td>
                <td class="text-center">
                    <a href="#" title=""><i class="fa fa-fw fa-edit"></i></a>
                    <a href="#" title=""><i class="fa fa-fw fa-trash"></i></a>
                </td>
                <td>
                    <center>
                        <input type="checkbox" class="toggle-admin-status" admin-id="{{ $account['admin_id'] }}" <?= $account['admin_active']==1?'checked':''?> />
                    </center>
                </td>
                <td>{{ $account['admin_username'] }}</td>
                <td>{{ $account['admin_role_title'] }}</td>
                <td>{{ date('H:i d/m/Y', strtotime($account['admin_last_activity'])) }}</td>
                <td>{{ date('H:i d/m/Y', strtotime($account['created_at'])) }}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.toggle-admin-status').bootstrapToggle({
            on          : "On",
            off         : "Off",
            onstyle     : "success",
            offstyle    : "default",
            size        : "mini",
            width       : "50"
        });
        
        var table = $('#table-admins').dataTable({
            "aoColumnDefs": [{
                "aTargets": [0, 1, 2],
                "bSortable": false
            }],
            "pagingType"    : "full_numbers",
            "lengthMenu"    : [[10, 20, 50, 100, -1], [10, 20, 50, 100, "<?= trans('layout.all')?>"]],
            'language'      : {
                'emptyTable'        : '<center><b>No data!!</b></center>',
                'lengthMenu'        : '<i class="fa fa-fw fa-list"></i>&nbsp;&nbsp; _MENU_',
                'search'            : '<i class="fa fa-fw fa-search"></i>&nbsp; _INPUT_',
                'searchPlaceholder' : '<?= trans('layout.search')?> ....',
                'zeroRecords'       : '<center><b><?= trans('layout.no-data')?>!!</b></center>',
                "info"              : '',
                "infoFiltered"      : '',
                "infoEmpty"         : ''
            }
        });
        
        $('#table-admins').after('<input type="checkbox" id="check-all-admin" style="margin-left:8px; vertical-align: top" /> <a id="delete-admin" href="#"><i class="fa fa-fw fa-trash"></i> <?= trans('admin-account.check-all-delete')?></a>');
    });
</script>
@endsection