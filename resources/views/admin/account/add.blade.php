@extends('layout.admin')

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('layout.btn-back') }}" href="{{ get_previous_link() }}">
            <i class="fa fa-fw fa-chevron-circle-left"></i>
        </a>
        {{ trans('admin-account.add') }}
    </legend>
    
    @if(count($errors) > 0)
    <div class="col-sm-12 col-xs-12 margin-5">
        <div class="alert alert-danger">
            <ul>
                @foreach($errors as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <form method="post">
        {!! csrf_field() !!}
        
        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-12 col-xs-12">
                <b>{{ trans('admin-account.username') }}</b>
            </div>
        </div>

        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-6 col-xs-12 div-line">
                <input type="text" class="form-control input-sm" name="admin_username" placeholder="{{ trans('admin-account.username') }}..." value="{{ $input['admin_username'] or '' }}" />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-12 col-xs-12">
                <b>{{ trans('admin-account.password') }}</b>
            </div>
        </div>

        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-6 col-xs-12 div-line">
                <input type="password" class="form-control input-sm" name="admin_password" placeholder="{{ trans('admin-account.password') }}..." value="{{ $input['admin_password'] or '' }}" />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-12 col-xs-12">
                <b>{{ trans('admin-account.confirm-password') }}</b>
            </div>
        </div>

        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-6 col-xs-12 div-line">
                <input type="password" class="form-control input-sm" name="admin_confirm_password" placeholder="{{ trans('admin-account.confirm-password') }}..." value="{{ $input['admin_confirm_password'] or '' }}" />
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-12 col-xs-12">
                <b>{{ trans('admin-account.prefix') }}</b>
            </div>
        </div>

        <div class="col-sm-12 col-xs-12 margin-5 no-padding">
            <div class="col-sm-6 col-xs-12 div-line">
                <select class="form-control input-sm" name="admin_role_id" placeholder="{{ trans('admin-account.prefix') }} ...">
                    @foreach($roles as $role)
                    @if(isset($input['admin_role_id']) && $input['admin_role_id'] == $role['admin_role_id'])
                    <option selected value="{{ $role['admin_role_id'] }}">{{ $role['admin_role_title'] }}</option>
                    @else
                    <option value="{{ $role['admin_role_id'] }}">{{ $role['admin_role_title'] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-20 text-right">
            <a href="<?= route('admin-product')?>" class="btn btn-default btn-no-radius">{{ trans('layout.cancel') }}</a>
            <button type="submit" class="btn btn-primary btn-no-radius">{{ trans('admin-account.add') }}</button>
        </div>
    </form>
    <legend></legend>
</div> 
@endsection