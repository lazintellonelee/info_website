@extends('layout.admin')

@section('head_link')
<link rel="stylesheet" type="text/javascript" href="<?= url('public/plugins/datatable/css/dataTables.bootstrap.css')?>" />
@endsection

@section('inline_script')
<script type="text/javascript" src="<?= url('public/js/functions.js')?>"></script>
<script type="text/javascript" src="<?= url('public/plugins/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" src="<?= url('public/plugins/datatable/js/dataTables.bootstrap.js')?>"></script>
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('admin-news-category.title-add') }}"  href=""><i class="fa fa-fw fa-plus-circle"></i></a>
        {{ trans('admin-news-category.list-news-category') }}
    </legend>
    
    @foreach(array('add','edit','delete') as $item)
        @if(Session::has($item))
            <div class="col-sm-12 col-xs-12 no-padding margin-5">
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get($item) }}
                </div>
            </div>
        @endif
    @endforeach
    
    <div id="container_form_delete" class="hidden"></div>
    
    <table class="table table-striped" id="table-news-category">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th>{{ trans('admin-news-category.th-name') }}</th>
                <th>{{ trans('admin-news-category.th-parent') }}</th>
                <th>{{ trans('admin-news-category.th-created') }}</th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
                <td style="vertical-align: middle">
                    <input type="checkbox" />
                </td>
                <td style="vertical-align: middle">
                    <a class="btn btn-xs btn-default" href="">Edit</a>
                    <a class="btn btn-xs btn-danger" href="">Delete</a>
                </td>
                <td style="vertical-align: middle">Thể Thao</td>
                <td style="vertical-align: middle">No Parent</td>
                <td style="vertical-align: middle">asd</td>
            </tr>
            
            <tr>
                <td style="vertical-align: middle">
                    <input type="checkbox" />
                </td>
                <td style="vertical-align: middle">
                    <a class="btn btn-xs btn-default" href="">Edit</a>
                    <a class="btn btn-xs btn-danger" href="">Delete</a>
                </td>
                <td style="vertical-align: middle">Giáo dục</td>
                <td style="vertical-align: middle">Không danh mục cha</td>
                <td style="vertical-align: middle">asd</td>
            </tr>
            
            <tr>
                <td style="vertical-align: middle">
                    <input type="checkbox" />
                </td>
                <td style="vertical-align: middle">
                    <a class="btn btn-xs btn-default" href="">Edit</a>
                    <a class="btn btn-xs btn-danger" href="">Delete</a>
                </td>
                <td style="vertical-align: middle">Bóng đá</td>
                <td style="vertical-align: middle">Thể thao</td>
                <td style="vertical-align: middle">asd</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#table-news-category').dataTable({
            "aoColumnDefs": [{
                "aTargets": [0, 1],
                "bSortable": false
            }],
            "pagingType"    : "full_numbers",
            "lengthMenu"    : [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
            'language'      : {
                'emptyTable'        : '<center><b>No data!!</b></center>',
                'lengthMenu'        : '_MENU_',
                'search'            : '_INPUT_',
                'searchPlaceholder' : 'Search ....',
                'zeroRecords'       : '<center><b>No data!!</b></center>',
                "info"              : '',
                "infoFiltered"      : '',
                "infoEmpty"         : ''
            }
        });
    });
</script>
@endsection