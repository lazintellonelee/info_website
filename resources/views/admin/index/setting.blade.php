@extends('layout.admin')

@section('head_link')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" />
@endsection

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.js') }}"></script>
@endsection

@section('content')
<div class="row">
    @foreach(array('add','edit','delete') as $item)
        @if(Session::has($item))
            <div class="col-sm-12 col-xs-12 no-padding margin-5">
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get($item) }}
                </div>
            </div>
        @endif
    @endforeach
    
    <div class="col-sm-12 col-xs-12 no-padding margin-5">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#admin_configuration" aria-controls="home" role="tab" data-toggle="tab">{{ trans('layout.admin-configuration') }}</a></li>
        @if(array_get(get_admin_info(), 'admin_role_name') == 'all')
        <li role="presentation"><a href="#home_configuration" aria-controls="profile" role="tab" data-toggle="tab">{{ trans('layout.home-configuration') }}</a></li>
        @endif
    </ul>
    
    <div class="tab-content">
        <!-- Admin Configuration -->
        <div role="tabpanel" class="tab-pane active" id="admin_configuration">
            <form method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="admin_configuration" value="1" />
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-globe"></i> {{ trans('layout.language') }}</b>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <select type="text" class="form-control input-sm" name="language">
                            @foreach(get_array_language() as $key => $value)
                            @if($key === app()->getLocale())
                            <option selected value="{{ $key }}">{{ $value }}</option>
                            @else
                            <option value="{{ $key }}">{{ $value }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-file-image-o"></i> {{ trans('layout.theme') }}</b>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <select type="text" class="form-control input-sm" name="theme">
                            @foreach(get_array_theme() as $key => $value)
                            @if(request()->cookie('admin_theme') && $key == request()->cookie('admin_theme'))
                            <option selected value="{{ $key }}">{{ $value }}</option>
                            @else
                            <option value="{{ $key }}">{{ $value }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-font"></i> Font awesome</b>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <button type="button" id="update_font_awesome_icon" class="btn btn-primary btn-sm btn-full-size">{{ trans('layout.btn-update') }}</button>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 margin-5 text-right">
                    <button class="btn btn-primary btn-no-radius">{{ trans('layout.btn-save') }}</button>
                </div>
            </form>
        </div>
        <!-- Admin Configuration -->
        
        <!-- Home Configuration -->
        @if(array_get(get_admin_info(), 'admin_role_name') == 'all')
        <div role="tabpanel" class="tab-pane" id="home_configuration">
            <form method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="home_configuration" value="1" />

                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-font"></i> {{ trans('layout.home-config-title') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_configuration_title" placeholder="{{ trans('layout.home-config-title') }} ..." value="{{ $input['home_configuration_title'] or $home_config['home_configuration_title'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        @if($home_config['home_configuration_shortcut'] == '')
                        <b><i class="fa fa-fw fa-image"></i> </b>
                        @else
                        <img src="{{ url($home_config['home_configuration_shortcut']) }}" />
                        @endif
                        <b>{{ trans('layout.home-config-shortcut') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="file" class="form-control input-sm" name="home_configuration_shortcut" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-image"></i> {{ trans('layout.home-config-logo') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="file" class="form-control input-sm" name="home_configuration_logo" />
                    </div>
                    
                    <div class="col-sm-offset-2 col-xs-offset-4 col-sm-6 col-xs-6 margin-5">
                        @if($home_config['home_configuration_logo'] != '')
                        <img class="img-responsive" src="{{ url($home_config['home_configuration_logo']) }}" />
                        @endif
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 margin-5">
                    <legend>Meta</legend>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-code"></i> {{ trans('layout.home-config-meta-description') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_meta_description" placeholder="Meta description ..." value="{{ $input['home_configuration_meta_description'] or $home_config['home_configuration_meta_description'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-code"></i> {{ trans('layout.home-config-meta-keywords') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <textarea rows="5" class="form-control input-sm" name="home_meta_keywords" placeholder="Meta keywords ...">{{ $input['home_configuration_meta_keywords'] or $home_config['home_configuration_meta_keywords'] }}</textarea>
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-code"></i> {{ trans('layout.home-config-meta-author') }}</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_meta_author" placeholder="Meta author ..." value="{{ $input['home_configuration_meta_author'] or $home_config['home_configuration_meta_author'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-code"></i> {{ trans('layout.home-config-meta-refresh') }}</b>
                    </div>

                    <div class="col-sm-1 col-xs-1 div-line">
                        @if(isset($input))
                        <input type="checkbox" name="home_configuration_refresh_state" class="toggle-refresh-home" <?= isset($input['home_configuration_refresh_state'])?'checked':''?> /> 
                        @else
                        <input type="checkbox" name="home_configuration_refresh_state" class="toggle-refresh-home" <?= $home_config['home_configuration_refresh_state']==1?'checked':''?> /> 
                        @endif
                    </div>
                    
                    <div class="col-sm-5 col-xs-5">
                        <input type="number" disabled class="form-control input-sm" name="home_configuration_refresh_second" id="home_configuration_refresh_second" placeholder="Second ..." value="{{ $input['home_configuration_refresh_second'] or $home_config['home_configuration_refresh_second'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 margin-5">
                    <legend>{{ trans('layout.social-network') }}</legend>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-facebook"></i> Facebook</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_facebook" placeholder="Facebook ..." value="{{ $input['home_facebook'] or $home_config['home_facebook'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-google-plus"></i> Google+</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_google" placeholder="Google ..." value="{{ $input['home_google'] or $home_config['home_google'] }}" />
                    </div>
                </div>
                
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-4 div-line">
                        <b><i class="fa fa-fw fa-twitter"></i> Twitter</b>
                    </div>

                    <div class="col-sm-6 col-xs-6">
                        <input type="text" class="form-control input-sm" name="home_twitter" placeholder="Twitter ..." value="{{ $input['home_twitter'] or $home_config['home_twitter'] }}" />
                    </div>
                </div>
                    
                <div class="col-sm-12 col-xs-12 margin-5 text-right">
                    <button class="btn btn-primary btn-no-radius">{{ trans('layout.btn-save') }}</button>
                </div>
            </form>
        </div>
        @endif
        <!-- Home Configuration -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '#update_font_awesome_icon', function(event){
            var _this   = this;
            var http    = new XMLHttpRequest();
            var form    = new FormData();
            
            $(this).html('<i class="fa fa-refresh fa-spin"></i>');
            
            form.append('_token', $('meta[name="csrf-token"]').attr('content'));
            
            http.open('POST', "<?= route('admin-crawler-fa-icon')?>", true);
            http.onload = function(event)
            {
                $(_this).html('<?= trans('layout.btn-update')?>');
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    CallNoty('danger', result.error);
                    return true;
                }
                CallNoty('success', result.success);
                return true;
            };
            http.send(form);
        });
        
        $('.toggle-refresh-home').bootstrapToggle({
            on          : 'On',
            off         : 'Off',
            onstyle     : 'success',
            offstyle    : 'danger',
            width       : '80',
            size        : 'small'
        });
        
        <?php if(isset($input)):?>
        <?php if($input['home_configuration_refresh_state'] == 1):?>
        $('#home_configuration_refresh_second').prop('disabled', false);
        <?php endif;?>
        <?php else:?>
        <?php if($home_config['home_configuration_refresh_state'] == 1):?>
        $('#home_configuration_refresh_second').prop('disabled', false);
        <?php endif;?>
        <?php endif;?>
        
        $('.toggle-refresh-home').on('change', function(event){
            if($(this).prop('checked'))
            {
                $('#home_configuration_refresh_second').prop('disabled', false);
                return true;
            }
            $('#home_configuration_refresh_second').val('').prop('disabled', true);
            return true;
        });
        
        <?php if(isset($input['home_configuration'])):?>
        $('.nav-tabs a[href="#home_configuration"]').tab('show');
        <?php endif;?>
            
        <?php if(Session::has('home_configuration')):?>
        $('.nav-tabs a[href="#home_configuration"]').tab('show');
        <?php endif;?>
    });
</script>
@endsection