<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Login</title>
        <link rel="icon" href="<?= url('public/img/login-icon.png')?>" type="image/png" sizes="16x16">
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/bootstrap/css/bootstrap.css') ?>" />
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/font-awesome-4.4.0/css/font-awesome.css') ?>" />
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/checkbox/normalize.css')?>" />
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/checkbox/style.css')?>" />
        <link rel="stylesheet" type="text/css" href="<?= url('public/css/style.css') ?>" />
        <link rel="stylesheet" type="text/css" href="<?= url('public/css/login.css') ?>" />
    </head>

    <body>
        <!--login modal-->
        <div class="container">
            <div class="row">
                <div id="contentdiv" class="col-sm-offset-3 col-sm-6">
                    <h2>Login</h2>
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div>
                        <form method="post">
                            {!! csrf_field() !!}
                            <input name="username" type="text" placeholder="username" value="{{ $input['username'] or '' }}" />
                            <input name="password" type="password" placeholder="password" value="{{ $input['password'] or '' }}" />
                            <input id="remember" type="checkbox" name="remember" <?= isset($input['remember'])?'checked':''?> />
                            <label for="remember">Remember Me.</label>
                            <button style="margin-top:17px" type="submit" class="btn btn-primary btn-block btn-no-radius">Sign In</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?= url('public/plugins/jquery/jquery-2.1.4.js') ?>"></script>
        <script type="text/javascript" src="<?= url('public/plugins/bootstrap/js/bootstrap.js') ?>"></script>
    </body>
</html>