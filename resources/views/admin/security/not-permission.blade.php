@extends('layout.admin')

@section('content')
<div class="row">
    <div class="panel panel-danger">
        <div class="panel-heading"><i class="fa fa-fw fa-exclamation-triangle"></i> <b>{{ trans('layout.warning') }}:</b> {{ trans('layout.not-permission-title') }}!</div>
        <div class="panel-body">
            <i>{{ trans('layout.not-permission-text') }}</i>
        </div>
    </div>
</div>
@endsection