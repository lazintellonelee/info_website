@extends('layout.admin')

@section('head_link')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/datatable/css/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/magnific-popup/dist/magnific-popup.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/chosen/chosen-bootstrap.css') }}" />
@endsection

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/datatable/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/javascript-template/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/chosen/chosen.jquery.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <legend>
        {{ trans('admin-user.list-user') }}
    </legend>
    @foreach(array('add','edit','delete') as $item)
        @if(Session::has($item))
            <div class="col-sm-12 col-xs-12 no-padding margin-5">
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get($item) }}
                </div>
            </div>
        @endif
    @endforeach
    
    <div id="container_form_delete" class="hidden"></div>

    <table class="table table-hover table-responsive" id="table-users">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>{{ trans('admin-user.th-fullname') }}</th>
                <th>{{ trans('admin-user.th-email') }}</th>
                <th>{{ trans('admin-user.th-created-at') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($users)
                @foreach($users as $user)
                    <tr>
                        <td style="vertical-align: middle">
                            <input type="checkbox" class="check-user" user-id="" value="" />
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <a href="#" title="{{ trans('admin-user.title-delete') }}"><i class="fa fa-fw fa-trash"></i></a>
                        </td>
                        <td style="padding: 4.5px 0px 4.5px 0px; vertical-align: middle">
                            <center>
                                <img width="30px" class="img-responsive img-rounded" src="{{ $user['user_small_avatar'] }}" />
                            </center>
                        </td>
                        <td style="vertical-align: middle">
                            {{ $user['user_fullname'] }}
                        </td>
                        <td style="vertical-align: middle">
                            {{ $user['user_email'] }}
                        </td>
                        <td style="vertical-align: middle">
                            {{ date('H:i d/m/Y', strtotime($user['created_at'])) }}
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    <legend></legend>
    
    <script type="text/javascript">
        $(document).ready(function() {
            
            var table = $('#table-users').dataTable({
                "aoColumnDefs": [{
                    "aTargets": [0, 1, 2],
                    "bSortable": false
                }],
                "pagingType"    : "full_numbers",
                "lengthMenu"    : [[10, 20, 50, 100, -1], [10, 20, 50, 100, "<?= trans('layout.all')?>"]],
                'language'      : {
                    'emptyTable'        : '<center><b>No data!!</b></center>',
                    'lengthMenu'        : '<i class="fa fa-fw fa-list"></i>&nbsp;&nbsp; _MENU_',
                    'search'            : '<i class="fa fa-fw fa-search"></i>&nbsp; _INPUT_',
                    'searchPlaceholder' : '<?= trans('layout.search')?> ....',
                    'zeroRecords'       : '<center><b><?= trans('layout.no-data')?>!!</b></center>',
                    "info"              : '',
                    "infoFiltered"      : '',
                    "infoEmpty"         : ''
                }
            });
            
            $('#table-users').after('<input type="checkbox" id="check-all-product" style="margin-left:8px; vertical-align: top" /> <a id="delete-product" href="#"><i class="fa fa-fw fa-trash"></i> <?= trans('admin-product.check-all-delete')?></a>');
            
            /*
            $('.dataTables_length').parent('div').removeClass('col-sm-6').addClass('col-sm-2');
            $('.dataTables_filter').parent('div').removeClass('col-sm-6').addClass('col-sm-3');
            $('.dataTables_length').parent('div').before(tmpl('datatables_category_product', {}));
            */
            
            $('#table_product_category').chosen({
                width                           : '300px',
                height                          : '30px',
                placeholder_text_single         : '<?= trans('admin-product.th-category')?>',
                include_group_label_in_selected : true
            });
            
            $("#table_product_category").chosen().change(function(event){
                window.location = "<?= url()?>/admin/product/" + $(this).val();
                return true;
            });
            
            $(document).on('change', '.toggle-product-status', function(event){
                $('.toggle-product-status').not(this).attr('disabled', true);
                var _this           = this;
                var product_active  = $(this).prop('checked')?"1":"0";
                var product_id      = $(this).attr('product-id');
                var form            = new FormData();
                form.append('_token', '<?= csrf_token()?>');
                form.append('product_id', product_id);
                form.append('product_active', product_active);
                
                var http = new XMLHttpRequest();
                http.open('POST', '<?= url()?>/admin/product/change-active', true);
                http.onload = function(event)
                {
                    $('.toggle-product-status').not(_this).attr('disabled', false);
                    var result = JSON.parse(this.responseText);
                    if(result.error !== undefined)
                    {
                        $(_this).prop('checked', product_active==="1"?false:true);
                        CallNoty('danger', result.error);
                        return true;
                    }
                    CallNoty('success', result.success);
                    return true;
                };
                http.send(form);
            });
            
            $(document).on('click', '#check-all-product', function(event){
                CheckAll(event.target.id, 'check-product');
            });
            
            $(document).on('click', '#delete-product', function(event) {
                event.preventDefault();
                if($('input.check-product:checked').length === 0)
                {
                    alert('Chưa chọn gì hết mà');
                    return false;
                }
                
                var container   = document.getElementById('container_form_delete');
                var form        = document.createElement('form');
                    form.setAttribute('method', 'POST');
                    form.setAttribute('action', '<?= route('admin-product-delete')?>');
                var token       = document.createElement('input');
                    token.setAttribute('type', 'hidden');
                    token.setAttribute('name', '_token');
                    token.setAttribute('value', '<?= csrf_token()?>');
                    form.appendChild(token);
                    
                for(var i=0; i<$('input.check-product:checked').length; i++)
                {
                    var input = document.createElement('input');
                        input.setAttribute('name', 'product_id[]');
                        input.setAttribute('value', $($('input.check-product:checked')[i]).val());
                        form.appendChild(input);
                }
                
                container.appendChild(form);
                $('#container_form_delete form').submit();
                return true;
            });
        });
    </script>
</div>
@endsection