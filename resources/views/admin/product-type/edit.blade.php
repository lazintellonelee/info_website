@extends('layout.admin')

@section('inline_script')
<script type="text/javascript" src="{{ url('public/plugins/typeahead/typeahead.bundle.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('layout.btn-back') }}" href="{{ get_previous_link() }}">
            <i class="fa fa-fw fa-chevron-circle-left"></i>
        </a>
        {{ trans('admin-product-type.title-edit') }}
    </legend>
    <form method="post">
        @if(count($errors) > 0)
        <div class="col-sm-12 col-xs-12 margin-5">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        
        {!! csrf_field() !!}
        
        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12 div-line"><b>{{ trans('admin-product-type.th-name') }}</b></div>
        </div>

        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12">
                @if(isset($input))
                <input type="text" class="form-control input-sm" name="product_type_name" value="{{ $input['product_type_name'] or '' }}" />
                @else
                <input type="text" class="form-control input-sm" name="product_type_name" value="{{ $type['product_type_name'] or '' }}" />
                @endif
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12 div-line"><b>{{ trans('admin-product-type.th-parent') }}</b></div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12">
                <select class="form-control input-sm" name="product_type_parent_id">
                    <option value="0">{{ trans('admin-product-type.is-parent') }}</option>
                    @if(count($typies) > 0)
                        @foreach($typies as $_type)
                            @if(isset($input))
                                @if($input['product_type_parent_id'] == $_type['product_type_id'])
                                <option selected value="{{ $_type['product_type_id'] }}">{{ $_type['product_type_name'] }}</option>
                                @else
                                <option value="{{ $_type['product_type_id'] }}">{{ $_type['product_type_name'] }}</option>
                                @endif
                            @else
                                @if($type['product_type_parent_id'] == $_type['product_type_id'])
                                <option selected value="{{ $_type['product_type_id'] }}">{{ $_type['product_type_name'] }}</option>
                                @else
                                <option value="{{ $_type['product_type_id'] }}">{{ $_type['product_type_name'] }}</option>
                                @endif
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12 div-line"><b>Font Awsome Icon</b></div>
        </div>

        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12">
                <input type="text" class="form-control input-sm" id="product_type_fa_icon_typeahead"  />
            </div>
        </div>
        
        <input type="hidden" class="form-control input-sm" id="product_type_fa_icon" name="product_type_fa_icon" value="<?= isset($input)?['product_type_fa_icon']:$type['product_type_fa_icon']?>" />
        
        
        <div class="col-sm-12 col-xs-12 margin-20 text-right">
            <a href="<?= route('admin-product-type')?>" class="btn btn-default btn-no-radius">{{ trans('layout.cancel') }}</a>
            <button type="submit" class="btn btn-primary btn-no-radius">{{ trans('admin-product-type.title-edit') }}</button>
        </div>
    </form>
    <legend></legend>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var faicon = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                cache   : false,
                url     : "<?= url('public/json/fa-icon.json')?>",
                filter  : function (data) {
                    return $.map(data, function (fa) {
                        return {
                            name    : fa.name,
                            fa      : fa.fa
                        };
                    });
                }
            }
        });

        faicon.initialize();

        $('#product_type_fa_icon_typeahead').typeahead({
            hint: true
        },
        {
            name    : 'faicon',
            display : 'name',
            value   : 'fa',
            templates: {
                empty: [
                    '<div style="padding: 5px 10px 5px 10px">',
                    'No font awsome icon here',
                    '</div>'
                ].join('\n'),
                suggestion: function (fa) {
                    return '<div><i class="' + fa.fa + '"></i>&nbsp;&nbsp;' + fa.name + '</div>';
                }
            },
            source: faicon.ttAdapter()
        });
        
        $('#product_type_fa_icon_typeahead').bind('typeahead:select', function(ev, suggestion) {
            $('#product_type_fa_icon').val(suggestion.fa);
            return true;
        });
        
        <?php if(isset($input)):?>
        $('#product_type_fa_icon_typeahead').typeahead('val', "<?= str_replace('-', ' ', trim($input['product_type_fa_icon'], "fa "))?>");
        <?php else:?>
        $('#product_type_fa_icon_typeahead').typeahead('val', "<?= str_replace('-', ' ', trim($type['product_type_fa_icon'], "fa "))?>");
        <?php endif;?>
        
    });
</script>
@endsection