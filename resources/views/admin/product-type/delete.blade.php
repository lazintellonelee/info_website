@extends('layout.admin')

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('layout.btn-back') }}" href="{{ get_previous_link() }}">
            <i class="fa fa-fw fa-chevron-circle-left"></i>
        </a>
        {{ trans('admin-product-type.delete-label') }}
    </legend>
    <form method="post">
        {!! csrf_field() !!}
        <div class="col-sm-12 col-xs-12 no-padding margin-5">
            <div class="col-sm-12 col-xs-12 div-line">
                <ul>
                    @foreach($typies as $type)
                    <input type="hidden" name="product_type_id[]" value="{{ $type['product_type_id'] }}" />
                    <li>{{ $type['product_type_name'] }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        
        <div class="col-sm-12 col-xs-12 margin-20 text-right">
            <a href="<?= route('admin-product-type')?>" class="btn btn-default btn-no-radius">{{ trans('layout.cancel') }}</a>
            <button type="submit" name="accept_delete" class="btn btn-primary btn-no-radius">{{ trans('admin-product-type.btn-delete') }}</button>
        </div>
    </form>
    <legend></legend>
</div>
@endsection