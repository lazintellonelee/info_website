@extends('layout.admin')

@section('head_link')
<link rel="stylesheet" type="text/javascript" href="<?= url('public/plugins/datatable/css/dataTables.bootstrap.css')?>" />
@endsection

@section('inline_script')
<script type="text/javascript" src="<?= url('public/js/functions.js')?>"></script>
<script type="text/javascript" src="<?= url('public/plugins/datatable/js/jquery.dataTables.js')?>"></script>
<script type="text/javascript" src="<?= url('public/plugins/datatable/js/dataTables.bootstrap.js')?>"></script>
@endsection

@section('content')
<div class="row">
    <legend>
        <a title="{{ trans('admin-product-type.btn-add') }}"  href="{{ route('admin-product-type-add') }}"><i class="fa fa-fw fa-plus-circle"></i></a>
        {{ trans('admin-product-type.title') }}
    </legend>
    @foreach(array('add','edit','delete') as $item)
        @if(Session::has($item))
            <div class="col-sm-12 col-xs-12 no-padding margin-5">
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get($item) }}
                </div>
            </div>
        @endif
    @endforeach
    
    <div id="container_form_delete" class="hidden"></div>
    
    <table class="table table-striped" id="table-product-typies">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th>{{ trans('admin-product-type.th-name') }}</th>
                <th>{{ trans('admin-product-type.th-parent') }}</th>
                <th>{{ trans('admin-product-type.th-created') }}</th>
            </tr>
        </thead>
        <tbody>
            @if(count(@typies) > 0)
            @foreach($typies as $type)
            <tr>
                <td>
                    <input type="checkbox" class="check-product-type" value="{{ $type['product_type_id'] }}" />
                </td>
                <td class="text-center">
                    <a href="{{ route('admin-product-type-edit', array('id' => $type['product_type_id'])) }}" title="{{ trans('admin-product-type.title-edit') }}"><i class="fa fa-fw fa-edit"></i></a>
                    <a href="{{ route('admin-product-type-delete', array('id' => $type['product_type_id'])) }}" title="{{ trans('admin-product-type.title-delete') }}"><i class="fa fa-fw fa-trash"></i></a>
                </td>
                <td>
                    @if($type['product_type_fa_icon'] !== '')
                    <i class="{{ $type['product_type_fa_icon'] }}"></i>&nbsp;&nbsp;
                    @endif
                    {{ $type['product_type_name'] }}</td>
                <td>
                    @if($type['product_type_parent_id'] == '0')
                    {{ trans('admin-product-type.is-parent') }}
                    @else
                    {{ $type['product_type_parent_name'] }}
                    @endif
                </td>
                <td>{{ date('d/m/Y H:i:s', strtotime($type['created_at'])) }}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
    <legend></legend>
    
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#table-product-typies').dataTable({
                "aoColumnDefs": [{
                    "aTargets": [0, 1],
                    "bSortable": false
                }],
                "pagingType"    : "full_numbers",
                "lengthMenu"    : [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                'language'      : {
                    'emptyTable'        : '<center><b>No data!!</b></center>',
                    'lengthMenu'        : '_MENU_',
                    'search'            : '_INPUT_',
                    'searchPlaceholder' : 'Search ....',
                    'zeroRecords'       : '<center><b>No data!!</b></center>',
                    "info"              : '',
                    "infoFiltered"      : '',
                    "infoEmpty"         : ''
                }
            });
            
            $('#table-product-typies').after('<input type="checkbox" id="check-all-product-type" style="margin-left:8px; vertical-align: top" /> <a id="delete-product-type" href="#"><i class="fa fa-fw fa-trash"></i> <?= trans('admin-product-type.check-all-delete')?></a>');
            
            $(document).on('click', '#check-all-product-type', function(event) {
                CheckAll(event.target.id, 'check-product-type');
                return true;
            });
            
            $(document).on('click', '#delete-product-type', function(event) {
                event.preventDefault();
                if($('input.check-product-type:checked').length === 0)
                {
                    alert('Chưa chọn gì hết mà');
                    return false;
                }
                
                var container   = document.getElementById('container_form_delete');
                var form        = document.createElement('form');
                    form.setAttribute('method', 'POST');
                    form.setAttribute('action', '<?= route('admin-product-type-delete')?>');
                var token       = document.createElement('input');
                    token.setAttribute('type', 'hidden');
                    token.setAttribute('name', '_token');
                    token.setAttribute('value', '<?= csrf_token()?>');
                    form.appendChild(token);
                    
                for(var i=0; i<$('input.check-product-type:checked').length; i++)
                {
                    var input = document.createElement('input');
                        input.setAttribute('name', 'product_type_id[]');
                        input.setAttribute('value', $($('input.check-product-type:checked')[i]).val());
                        form.appendChild(input);
                }
                
                container.appendChild(form);
                $('#container_form_delete form').submit();
                return true;
            });
        });
    </script>
</div>
@endsection