<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="<?= url('public/img/management-statistics-icon.png')?>" type="image/png" sizes="16x16">
        <title>Free Bootstrap Admin Template : Binary Admin</title>
        <!-- BOOTSTRAP STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/bootstrap/css/bootstrap.css') ?>" />
        <!-- FONTAWESOME STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/font-awesome-4.4.0/css/font-awesome.css') ?>" />
        <!-- MORRIS CHART STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/morris/morris-0.4.3.min.css')?>" />
        <!-- ANIMATE -->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/animate/animate.css')?>" />
        
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/checkbox/normalize.css')?>" />
        
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/checkbox/style.css')?>" />
        
        @yield('head_link')
        
        <link rel="stylesheet" type="text/css" href="<?= url('public/css/style.css')?>" />
        <!-- GOOGLE FONTS-->
        <!-- <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans' /> -->
        
        <!-- JQUERY SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/jquery/jquery-2.1.4.js') ?>"></script>
        
        <?= get_theme()?>
        
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="modal fade" id="modal_signup" modal-backdrop="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal_title">Sign In</h4>
                            </div>
                            <div class="modal-body row" id="field_sign_in">
                                <div class="col-sm-12 col-xs-12 no-padding">
                                    <div class="col-sm-6 col-xs-12 no-padding">
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <h4>Use other accounts</h4>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            You can also sign in using your Facebook Account or Google Account.
                                        </div>
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <a href="{{ $link_login_with_fb }}" class="btn btn-primary btn-no-radius btn-full-size"><i class="fa fa-fw fa-facebook"></i> Login with Facebook</a>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <a class="btn btn-danger btn-no-radius btn-full-size"><i class="fa fa-fw fa-google"></i> Login with Google</a>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6 col-xs-12 no-padding border-left">
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <h4>Use your account</h4>
                                        </div>
                                        
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <input type="text" class="form-control input-no-radius" placeholder="Username ..." />
                                        </div>
                                        
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <input type="password" class="form-control input-no-radius" placeholder="Password ..." />
                                        </div>
                                        
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <input id="remember" type="checkbox" name="remember" <?= isset($input['remember']) ? 'checked' : ''?> />
                                            <label for="remember">Remember Me.</label>
                                        </div>
                                        
                                        <div class="col-sm-12 col-xs-12 margin-5">
                                            <button class="btn btn-info btn-no-radius btn-full-size">Sign In</button>
                                        </div>
                                        
                                        <div class="col-sm-12 col-xs-12 margin-10 text-right">
                                            <a href="#" class="margin-20">Forgot your password?</a><br/>
                                            <a href="#" id="new_user" class="margin-20">New User?</a>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix margin-20"></div>
                                </div>
                            </div>
                            
                            <div class="modal-body row hidden"  id="field_sign_up">
                                <div class="col-sm-12 col-xs-12">
                                    <b>Fullname</b>
                                </div>
                                <div class="col-sm-12 col-xs-12 margin-5">
                                    <input class="form-control input-no-radius" placeholder="Fullname ..." />
                                </div>
                                
                                <div class="col-sm-12 col-xs-12">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-12 col-xs-12 margin-5">
                                    <input class="form-control input-no-radius" placeholder="Email ..." />
                                </div>
                                
                                <div class="col-sm-12 col-xs-12">
                                    <b>Password</b>
                                </div>
                                <div class="col-sm-12 col-xs-12 margin-5">
                                    <input class="form-control input-no-radius" placeholder="Password ..." />
                                </div>
                                
                                <div class="col-sm-12 col-xs-12">
                                    <b>Password confirm</b>
                                </div>
                                <div class="col-sm-12 col-xs-12 margin-5">
                                    <input class="form-control input-no-radius" placeholder="Password confirm ..." />
                                </div>
                                
                                <div class="col-sm-12 col-xs-12 margin-10">
                                    <button class="btn btn-info btn-no-radius btn-full-size">Sign Up</button>
                                </div>
                                
                                <div class="col-sm-12 col-xs-12 margin-5 text-right">
                                    <a href="#" id="have_account" class="margin-20">I have account.</a><br/>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
        </div>
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('#modal_signup').modal({
                    show        : true,
                    keyboard    : false,
                    backdrop    : false
                });
                $(document).on('click', '#new_user', function(event){
                    event.preventDefault();
                    $('#modal_title').html('Sign up');
                    $('#field_sign_in').addClass('hidden');
                    $('#field_sign_up').removeClass('hidden');
                });
                $(document).on('click', '#have_account', function(event){
                    event.preventDefault();
                    $('#modal_title').html('Sign In');
                    $('#field_sign_in').removeClass('hidden');
                    $('#field_sign_up').addClass('hidden');
                });
            });
        </script>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- BOOTSTRAP SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/bootstrap/js/bootstrap.js') ?>"></script>
        <!-- MORRIS CHART SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/morris/raphael-2.1.0.min.js')?>"></script>
        <script type="text/javascript" src="<?= url('public/plugins/morris/morris.js')?>"></script>
        <!-- NOTY -->
        <script type="text/javascript" src="<?= url('public/plugins/noty/js/noty/packaged/jquery.noty.packaged.js')?>"></script>
        <!-- FUNCTIONS -->
        <script type="text/javascript" src="<?= url('public/js/functions.js')?>"></script>
        <!-- CUSTOM SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/js/admin/custom.js')?>"></script>
        
        @yield('inline_script')
    </body>
</html>
