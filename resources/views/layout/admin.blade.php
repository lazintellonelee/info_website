<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="<?= url('public/img/management-statistics-icon.png')?>" type="image/png" sizes="16x16">
        <title>Free Bootstrap Admin Template : Binary Admin</title>
        <!-- BOOTSTRAP STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/bootstrap/css/bootstrap.css') ?>" />
        <!-- FONTAWESOME STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/font-awesome-4.4.0/css/font-awesome.css') ?>" />
        <!-- MORRIS CHART STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/morris/morris-0.4.3.min.css')?>" />
        <!-- ANIMATE -->
        <link rel="stylesheet" type="text/css" href="<?= url('public/plugins/animate/animate.css')?>" />
        <!-- CUSTOM STYLES-->
        <link rel="stylesheet" type="text/css" href="<?= url('public/css/admin/custom.css')?>" />
        
        @yield('head_link')
        
        <link rel="stylesheet" type="text/css" href="<?= url('public/css/style.css')?>" />
        <!-- GOOGLE FONTS-->
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans' />
        
        <!-- JQUERY SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/jquery/jquery-2.1.4.js') ?>"></script>
        
        <?= get_theme()?>
        
    </head>
    <body>
        <div id="wrapper">
            @include('layout.partial.admin-menu-horizontal')
            <!-- /. NAV TOP  -->
            @include('layout.partial.admin-menu-vertical') 
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- BOOTSTRAP SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/bootstrap/js/bootstrap.js') ?>"></script>
        <!-- METISMENU SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/js/admin/jquery.metisMenu.js')?>"></script>
        <!-- MORRIS CHART SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/plugins/morris/raphael-2.1.0.min.js')?>"></script>
        <script type="text/javascript" src="<?= url('public/plugins/morris/morris.js')?>"></script>
        <!-- NOTY -->
        <script type="text/javascript" src="<?= url('public/plugins/noty/js/noty/packaged/jquery.noty.packaged.js')?>"></script>
        <!-- FUNCTIONS -->
        <script type="text/javascript" src="<?= url('public/js/functions.js')?>"></script>
        <!-- CUSTOM SCRIPTS -->
        <script type="text/javascript" src="<?= url('public/js/admin/custom.js')?>"></script>
        
        @yield('inline_script')
    </body>
</html>
