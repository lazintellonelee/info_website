<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <a  href="#"><i class="fa fa-dashboard"></i> {{ trans('layout.dashboard') }}</a>
            </li>
            
            @if(array_get(get_admin_info(), 'admin_role_name') == 'all' ||
                array_get(get_admin_info(), 'admin_role_name') == 'product_manager')
            <li>
                <a href="#"><i class="fa fa-archive"></i> {{ trans('layout.product-management') }}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= route('admin-product')?>">{{ trans('layout.list-product') }}</a>
                    </li>
                    <li>
                        <a href="<?= route('admin-product-type')?>">{{ trans('layout.list-product-type') }}</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if(array_get(get_admin_info(), 'admin_role_name') == 'all' ||
                array_get(get_admin_info(), 'admin_role_name') == 'news_manager')
            <li>
                <a href="#"><i class="fa fa-newspaper-o"></i> {{ trans('layout.news-management') }}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">{{ trans('layout.list-news') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('admin-news-category') }}">{{ trans('layout.list-news-category') }}</a>
                    </li>
                    <li>
                        <a href="#">{{ trans('layout.list-news-vote') }}</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if(array_get(get_admin_info(), 'admin_role_name') == 'all' ||
                array_get(get_admin_info(), 'admin_role_name') == 'user_manager')
            <li>
                <a href="{{ route('admin-user') }}"><i class="fa fa-users"></i> {{ trans('layout.user-management') }}</a>
            </li>
            @endif
            
            @if(array_get(get_admin_info(), 'admin_role_name') == 'all')
            <li>
                <a href="{{ route('admin-account') }}"><i class="fa fa-lock"></i> {{ trans('layout.administrator') }}</a>
            </li>
            @endif
            
            <li>
                <a href="<?= route('admin-setting')?>"><i class="fa fa-wrench"></i> {{ trans('layout.setting') }}</a>
            </li>
            
            <li>
                <a href="<?= url()?>"><i class="fa fa-eye"></i> {{ trans('layout.view-website') }}</a>
            </li>
        </ul>
    </div>
</nav> 